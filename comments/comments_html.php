<?php
// for pagination
$comments_page = $_GET['comments_page'];
$my_comments_counter=0;

global $comments_per_page;
global $comments_page;
global $first_comment;
global $last_comment;
global $my_comments_counter;
		
if(!isset($comments_page)){
	$comments_page = ceil(count($comments) / $comments_per_page);
}
$first_comment = count($comments)-($comments_per_page*$comments_page); 
$last_comment = $first_comment + $comments_per_page;
// for pagination
?>

<?php
// count any existent errors; $comment_errors will be used later to know if form will be visible or not.
$comment_errors = 0;
if(is_array($addedComment)):
foreach($addedComment as $errorMessage):
	$comment_errors ++;
endforeach;
endif;
?>

<div class="commentsystem">


<div class="comments_area_title">Comments <a name="comments_anchor" id="comments_anchor"></a><?php echo 'page '.$comments_page.' of '.(ceil(count($comments) / $comments_per_page));?></div>



<div class="add_comment_button" id="add_comment_button" onclick="JavaScript:document.getElementById('tnt_comments_submit_form').style.display='block';document.getElementById('add_comment_button').style.display='none';" <?php if($comment_errors!=0){echo'style="display:none;"';}?>>Click here to add a comment</div>

<form id="tnt_comments_submit_form" method="post" action="<?php echo $postLocation.'#comments_anchor';?>" onsubmit="rememberfields()" <?php if($comment_errors==0){echo'style="display:none;"';}?>>
		
	<label for="commentform_yourname">Your Name:</label><br />
	<input type="text" name="authorname" id="commentform_yourname" value="<?php echo $_COOKIE['tntname']; ?>" class="comment_form_text_box"/><br /><br />
	
	<?php if($require_email): ?>
	<label for="commentform_email">Email Address:</label><br />
	<input type="text" name="email" id="commentform_email" value="<?php echo $_COOKIE['tntemail']; ?>" class="comment_form_text_box"/>
	<br />
	<br />
	<?php endif; ?>


	<?php if ($use_verification_image): ?>
	<label for="commentform_verification">Image verification:</label><br />
	<input name="verification" type="text" id="commentform_verification" class="comment_form_text_box"/>
	<br />
	<img src="<?php echo $commentsystemfolder; ?>comments_verificationimage.php?r=<?php echo md5(uniqid(time())); ?>" alt="image verification" name="commentform_verificationimage" width="100" height="20" id="commentform_verificationimage"/><br /><br />
	<?php endif; ?>
		<label for="commentform_message">Message:</label><br />
		<textarea name="message" cols="20" rows="5" class="comment_form_text_area" id="commentform_message"><?php echo $post_message ?></textarea><br />
		<input type="hidden" name="posted" value="true" />
		<input name="Button" type="button" class="comment_form_submit" value="Cancel"  onclick="JavaScript:document.getElementById('tnt_comments_submit_form').style.display='none';document.getElementById('add_comment_button').style.display='block';" />&nbsp;
		<input type="submit" class="comment_form_submit" value="Add Comment" />
</form>


<?php if($commentDeleted): ?>
<div class="success">Comment Deleted!</div>
<?php endif; ?>

<?php
if(is_array($addedComment)):
foreach($addedComment as $errorMessage):
?>
<div class="error">Error: <?php echo $errorMessage ?></div>
<?php
endforeach;
endif;
?>

<?php if($commentAdded): ?>
<div class="success">Comment Added!</div>
<?php endif; ?>
<script type="text/javascript">
function rememberfields(){
	var name = document.getElementById('commentform_yourname').value;
	document.cookie = "tntname=" + escape(name) + "; path=/";
	<?php if($require_email): ?>
	var email = document.getElementById('commentform_email').value;
	document.cookie = "tntemail=" + escape(email) + "; path=/";
	<?php endif; ?>
	return true;
}
</script>

<?php if($commentEdited): ?>
<div class="success">The changes that have been made have been successfully applied.</div><br />
<?php endif; ?>





<!-- IF THERE ARE NO COMMENTS -->
<?php if($noComments): ?>
There are currently <b>0</b> comments to display.<br /><br />
<?php endif; ?>


<?php if($isadmin): ?>
<div id="inlineCommentEditor" style="display:none;">
	<form method="post" action="<?php echo $postLocation.'#comments_anchor';?>" class="editing_form">
		<input type="hidden" name="tntcommentid" id="inlinetntcommentid" value="" />
		<input type="hidden" name="tntactiontype" value="inlineEditor" />
		<textarea name="tntnewMessage" id="tntnewMessage" class="editing_comment_form_text_area"></textarea><br />
		<input type="submit" value="Save Changes" class="comment_form_submit" />
		<input type="button" value="Cancel" onclick="cancelInlineEdit()" class="comment_form_submit" />
	</form>
</div>
<script type="text/javascript"> 
var editingComment = false;
var originalMessage = "";
var messageContainer;
function initCommentEditForm(commentId){
    if (editingComment){
        alert("Error: You must finish the comment that you are currently editing before moving on to another."); 
        return false;
    }
    editingComment = true;
    var inlineEditor = document.getElementById('inlineCommentEditor');
    if (inlineEditor == null) return false;
    messageContainer = document.getElementById ('tntmessage' + commentId);
    originalMessage = messageContainer.innerHTML.replace(/^\s*|\s*$/g,"");
    //set up the form
    document.getElementById('inlinetntcommentid').value = commentId; 
	
	//sets variables for internet explorer and netscape
	var ie = document.all ? 1 : 0
	var ns = document.layers ? 1 : 0
	// setting the value of the comment to be edited, it needd to be done difrently in different browsers
	if(ie){
		document.getElementById('tntnewMessage').value = originalMessage.replace(/\<[bB][rR](( \/)?)\>/g,"\r\n"); 
	}else{
		document.getElementById('tntnewMessage').innerHTML = originalMessage.replace(/\<[bB][rR](( \/)?)\>/g,"\r\n"); 
	}
	
    messageContainer.innerHTML = inlineEditor.innerHTML;
    return false; 
}
function cancelInlineEdit(){
    if (messageContainer == null) return false;
    messageContainer.innerHTML = originalMessage;
    editingComment = false;
    return false;
}
</script>
<?php endif; ?>
<!-- LIST OF ALL THE COMMENTS -->
<?php if ($areComments): ?>
<div class="commentslist">
	<?php foreach ($comments as $comment): ?>
	<?php global $my_comments_counter; $my_comments_counter ++;?>
	<?php if($my_comments_counter > $first_comment and $my_comments_counter<=$last_comment){?>
	<div class="comment">
		<div class="meta">
			<div class="author_name">
			<?php if($isadmin && $comment->authorEmail != 'none'){ echo '<a href="mailto:' . $comment->authorEmail . '">'; } ?>
			<?php echo $comment->authorName; ?>
			<?php if($isadmin && $comment->authorEmail != 'none'){ echo '</a>'; } ?>
			</div>

			<div class="comment_details">
			Posted <?php echo timeago($comment->timestamp); ?>
			<?php if($isadmin): ?>
			// <a href="<?php echo makeurl($uriWithVars, 'tntid=' . $comment->id . '&amp;tntaction=delete'); ?>#comments_anchor">Delete</a>
			, <a href="#comments_anchor" onclick="return initCommentEditForm('<?php echo $comment->id; ?>')">Edit</a>
			<?php endif; ?>
			</div>
		</div>
		<div class="comment_text" id="tntmessage<?php echo $comment->id; ?>">
		<?php echo $comment->message ?>
		</div>
	</div>
	<?php ;}?>
	<?php endforeach; ?>
</div>
<?php endif; ?>



<!-- pagination -->
<div class="pagination4">
<?php 
$total_pages = ceil(count($comments)/$comments_per_page);

// paginations after current page
if($total_pages-$comments_page < 4){
	$paginations_after = 8-($total_pages-$comments_page);
	} 
	else {
		$paginations_after = 4;
		}
if($comments_page<=4){
	$paginations_after = $comments_page-1;
}

// paginations before curent page
if($total_pages<=9){
	$paginations_before = $total_pages - $comments_page;
} else {
	$paginations_before=8-$paginations_after;
	}
	
$pagination_start = $comments_page+$paginations_before;
$pagination_end = $comments_page-$paginations_after;



//  if comments_page is first variable in address bar then it starts with '?'
// else if there are more varialbes in address bar then before comments_page print &, 
if (count($_GET)==0){
	$var_char='?';}
	else {
		$var_char='&';	
	}
// if there is only one variable in address bar and comments_page is also set 
// then this is the only var and show '?' char for pagination links
if (count($_GET)==1 and isset($_GET['comments_page'])){
	$var_char='?';
	}



// the [<] arrow at the begining, active or disabled (&gt;)
if($paginations_before>0){
	echo '<a href="'.$uriWithVars.$var_char.'comments_page='.($comments_page+1).'#comments_anchor">&lt;</a>';		
} else {echo '<span class="disabled_pagination">&lt;</span>';}

// add all the pagination numbers between the [<] ... [>] arrows
for($i=0;$i<$total_pages;$i++){

	if(($total_pages - $i)<=$pagination_start and ($total_pages - $i)>=$pagination_end){
		if($comments_page==($total_pages - $i)){
			echo '<span class="active_link">'.($total_pages - $i).'</span>';
			} else {
			echo '<a href="'.$uriWithVars.$var_char.'comments_page='.($total_pages - $i).'#comments_anchor">'.($total_pages - $i).'</a>';
		}
	}
}

// the [>] arrow at the end, active or disabled (&gt;)
if($paginations_after>0){
	echo '<a href="'.$uriWithVars.$var_char.'comments_page='.($comments_page-1).'#comments_anchor">&gt;</a>';		
} else {echo '<span class="disabled_pagination">&gt;</span>';}
?>

</div>&nbsp;
<!-- ^ pagination -->

</div><!-- end of commentsystem -->