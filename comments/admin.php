<?php
require('settings.php');
if ($_POST['posted'] == 'true'){
	if ($_POST['username'] === $username && $_POST['password'] === $password){
		setcookie('tntcommentsysusername', $username, null, '/');
		setcookie('tntcommentsyspassword', md5($password), null, '/');
		$loginsuccess = true;
	}else{ $loginsuccess = false; }
}
$loggedin = false;
if ($_COOKIE['tntcommentsysusername'] == $username &&
	$_COOKIE['tntcommentsyspassword'] == md5($password)){
	$loggedin = true;
}
?>
<html>
<head>
	<title>Administrator Login</title>
	<style type="text/css">
	body { text-align: center; font-size: 12px; background-color: #f3f3f3; font-family: arial, verdana; }
	h1 { font-size: 14px; margin: 0 0 8px 0; padding: 0; }
	form { margin: 0; padding: 0; }
	#container { padding: 20px; border: 1px solid #d0d0d0; background-color: #ffffff; margin: 200px auto; width: 200px; text-align: left; }
	input[type=text], input[type=password] { width: 100% }
	input[type=submit] { margin-top: 6px; }
	.error { font-weight: bold; color: #800000; margin-bottom: 4px; }
	</style>
</head>
<body>
	<div id="container">
		<h1>Admin Login</h1>
		<?php if($loginsuccess || $loggedin){ ?>
		You have successfully logged in! Now that you are logged in,
		you will see links next to each comment with administrator
		controls.
		<br /><br />
		It is safe to navigate to away from this page.<br>
		Go to <a href="/">home page</a>?
		<?php }else{ ?>
		<?php if($loginsuccess === false): ?>
	  <div class="error">Invalid username or password!</div>
		<?php endif; ?>
		<form method="post" action="admin.php">
			<label for="username">Username:</label><br />
			<input type="text" name="username" id="username" /><br />
			<label for="password">Password:</label><br />
			<input type="password" name="password" id="password" /><br />
			<input type="hidden" name="posted" value="true" />
			<input type="submit" value="Login" />
		</form>
		<?php } ?>
	</div>
</body>
</html>