<?php

//Administrator username
$username = 'discover';

//Administrator password
$password = 'aloha';

//Administrator email address
//If this field is left blank, you will not get
//email notifications when comments are posted.
$email = 'iva.popova@discoverhiddenhawaiitours.com';

//the relative path (relative to this file) to the database folder
//you probably shouldn't have to change this
$dbDirectory = 'database\\';

//the minimum number of characters that a posted comment must have
$minimumMessageLength = 3;

//the maximum number of characters that a posted comment can have
$maximumMessageLength = 5000;

//number of comments to show on one page
$comments_per_page = 7;

//if you have a content management system that uses variables like "?id=10" in the url,
//and that specifies the distinct page, set this variable to that. If there are more than
//one, separate them by a comma like this: "id,category,idx".
$pageGetVars = 'id';

//Control the sorting of comments. You can use 'asc' or 'desc'
$sorting = 'desc';

//Require an email address to be entered by the person that comments?
//1: yes
//0: no
$require_email = 0;

//Use a verification image to prevent spam?
//1: yes
//0: no
$use_verification_image = 1;

//What is the url to the directory that houses the comment scripts?
//$scriptpath = 'http://mydomain.com/commentsystem/';
//If it is left blank, it will try to autodetect.
$scriptpath = '';

// block long words so that a very loooonnnnnnggg word will not get out of your layout
// set maximum chars to be in a word
$max_word_length = 40;

// would you like to filter html tags like <a href="http://site.com">site</a> inside comments? 
//1: yes
//0: no
$filter_html_tags = 1;

// would you like to filter bad words inside comments? 
//1: yes
//0: no
$filter_words = 1;

// list of words to be filtered. Words will be replaced with "...":
$bad_words = array('fuck you','fuck','WTF','shit','Shit','suck','sucks',' ass ','bitch','dick','porn','cialis','viagra');

// string to show instead of the above word when replacement is made:
$censor_replacement = ' *** ';

?>
