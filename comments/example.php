<html >
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


<link href="style.css" rel="stylesheet" type="text/css" />





<title>Comment System Usage Example</title>
</head>
<body>
	This is a sample page with an embedded comment form. All you have to do to
	add it to a page is add:<br /><br />
	<code>&lt;?php include('comments.php'); ?&gt;</code>
	<br /><br />
	I also took $_GET variables into account for specifying the page databases. Check
	it out: ('sort' in the links below is a dummy variable to show the concept)
	<ul>
		<li><a href="example.php?id=2&amp;sort=asc">example.php?id=2&amp;sort=asc</a> &lt;-- this and the next page will be treated as the same pages</li>
		<li><a href="example.php?id=2&amp;sort=desc">example.php?id=2&amp;sort=desc</a></li>
		<li><a href="example.php?id=3">example.php?id=3&amp;sort=asc</a> &lt;-- this and the next page will be treated as the same pages</li>
		<li><a href="example.php?id=3">example.php?id=3&amp;sort=desc</a> &lt;-- </li>
	</ul>
	This functionability can be changed by <code>$pageGetVars</code> in settings.php.
	<?php include('comments.php'); ?>
</body>
</html>