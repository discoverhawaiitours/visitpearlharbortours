Comments Script // www.adriantnt.com



About: 
########################################################### 
This script allows users to post comments on your web site.



Usage instructions:
########################################################### 
Upload the "comments" directory (in site root would be ok).
You need to set writing permissions to the directory "database" inside comments directory. For Linux permission code is 777.
Ask your host if you have problems with the directory permission, that directory needs to be writable.

Then include the file comments.php in any of the pages in site where you want comments to show up.
For example:

<?php include 'comments/comments.php';?>

Normally the script identifies the item to be rated by the URL of the page. System saves the data according to the URL of the page where comments are posted.
Optionally you can specify an ID of the item to be commented, the id is passed before including the comments.php file:

Let's say you want the system to save the comments for "video1" then this is how code should look on your site that displays video1:

<?php $comment_id="video1";?>
<?php include 'comments/comments.php';?>

The system doesn't use a database, all comments are stored inside the "database" directory inside plain files.
If you specified an id on the page where comments show then the file that stores the comments is like id.dat, for example video1.dat;
If you didn't specify an id of the comments on your pages then file that stores the comments is named as URL that shows the comments, for example:
products_product1.dat
(Slashes are replaced with underscore _ )

Open the file settings.php to change some preferences and options like admin email; admin will receive an email when a comment was posted.

To moderate the comments login to comments/admin.php then browse your pages, you will see a "delete" and "edit" link on all comments after you are logged.
Admin username and password is also set in settings.php.


About styling:
########################################################### 
The comments script has a css file ready to be used, if you want to use that file to use custom colors, borders, etc include this css file in your pages:
<link href="/scripts/comments/style.css" rel="stylesheet" type="text/css" />
If you have the comments uploaded in another folder than change the above path accordingly. 
That line needs to be places in head area, between <head> ... </head> area of your html code.



Comments script inside HTML pages:
########################################################### 
There is a way to include the php code inside .html or .htm pages, for instructions please read this page:
http://php.about.com/od/advancedphp/p/html_php.htm

You will need to create one .htaccess file, place it in the directory where you will have a html page containing comments and in .htaccess type these line(s), 
you only need one of them but it depends form server to server which one you can use, you could try them one by one or type them all in the .htaccess:

AddType application/x-httpd-php .html .htm .php
Addhandler application/x-httpd-php .html .htm .php
AddHandler x-httpd-php .html .htm .cgi .php 


Now you can have the <?php include 'comments/comments.php';?> code even in html pages.

---------------------------------------------------------------------
For any questions or problems contact www.adrianTNT.com

