var deviceIphone = "iphone";
var deviceIpod = "ipod";
var devicePlaystation = "playstation";
var deviceWap = "wap";
var deviceWinMob = "windows ce";
var enginePie = "wm5 pie";
var deviceIeMob = "iemobile";
var deviceS60 = "series60";
var deviceSymbian = "symbian";
var deviceS60 = "series60";
var deviceS70 = "series70";
var deviceS80 = "series80";
var deviceS90 = "series90";
var deviceBB = "blackberry";
var deviceAndroid = "android";
var deviceMidp = "midp";
var deviceWml = "wml";
var deviceBrew = "brew";
var devicePalm = "palm";
var engineXiino = "xiino";
var engineBlazer = "blazer"; //Old Palm
var devicePda = "pda";
var deviceNintendoDs = "nitro";
var engineWebKit = "webkit";
var engineNetfront = "netfront";
var manuSonyEricsson = "sonyericsson";
var manuericsson = "ericsson";
var manuSamsung1 = "sec-sgh";
var svcDocomo = "docomo";
var svcKddi = "kddi";
var svcVodafone = "vodafone";
var s60GetsMobile = true;
var iphoneIpodGetsMobile = true;
var uagent = navigator.userAgent.toLowerCase();
function DetectIphone()
{
   if (uagent.search(deviceIphone) > -1)
   {

      if (uagent.search(deviceIpod) > -1)
         return false;
      else 
         return true;
   }
   else
      return false;
}
function DetectIpod()
{
   if (uagent.search(deviceIpod) > -1)
      return true;
   else
      return false;
}
function DetectIphoneOrIpod()
{
   if (uagent.search(deviceIphone) > -1 ||
       uagent.search(deviceIpod) > -1)
       return true;
    else
       return false;
}
function DetectAndroid()
{
   if (uagent.search(deviceAndroid) > -1)
      return true;
   else
      return false;
}
function DetectAndroidWebKit()
{
   if (DetectAndroid())
   {
     if (DetectWebkit())
        return true;
     else
        return false;
   }
   else
      return false;
}
function DetectWebkit()
{
   if (uagent.search(engineWebKit) > -1)
      return true;
   else
      return false;
}
function DetectS60OssBrowser()
{
   if (DetectWebkit())
   {
     if ((uagent.search(deviceS60) > -1 || 
          uagent.search(deviceSymbian) > -1))
        return true;
     else
        return false;
   }
   else
      return false;
}
function DetectSymbianOS()
{
   if (uagent.search(deviceSymbian) > -1 ||
       uagent.search(deviceS60) > -1 ||
       uagent.search(deviceS70) > -1 ||
       uagent.search(deviceS80) > -1 ||
       uagent.search(deviceS90) > -1)
      return true;
   else
      return false;
}
function DetectBlackBerry()
{
   if (uagent.search(deviceBB) > -1)
      return true;
   else
      return false;
}
function DetectWindowsMobile()
{
   if (uagent.search(deviceWinMob) > -1 ||
       uagent.search(deviceIeMob) > -1 ||
       uagent.search(enginePie) > -1)
      return true;
   else
      return false;
}
function DetectPalmOS()
{
   if (uagent.search(devicePalm) > -1 ||
       uagent.search(engineBlazer) > -1 ||
       uagent.search(engineXiino) > -1)
      return true;
   else
      return false;
}
function SetS60GetsMobile(setMobile)
{
   s60GetsMobile = setMobile;
};
function SetS60GetsMobile(setMobile)
{
   iphoneIpodGetsMobile = setMobile;
};
function DetectSmartphone()
{
   if (DetectIphoneOrIpod())
      return true;
   if (DetectS60OssBrowser())
      return true;
   if (DetectSymbianOS())
      return true;
   if (DetectWindowsMobile())
      return true;
   if (DetectBlackBerry())
      return true;
   if (DetectPalmOS())
      return true;
   return false;
};
function DetectMobileQuick()
{
   if (uagent.search(deviceWap) > -1   || 
	uagent.search(deviceMidp) > -1 ||
	uagent.search(deviceWml) > -1  ||
	uagent.search(deviceBrew) > -1  )
   {
      return true;
   }
   if (DetectSmartphone())
      return true;
   if (uagent.search(engineNetfront) > -1)
      return true;
   if (uagent.search(devicePlaystation) > -1)
      return true;
   if (uagent.search(devicePda) > -1)
      return true;
   return false;
};

function DetectMobileLonger()
{
   if (DetectMobileQuick())
      return true;
   if (uagent.search(svcDocomo) > -1)
      return true;
   if (uagent.search(svcKddi) > -1)
      return true;
   if (uagent.search(deviceNintendoDs) > -1)
      return true;
   if (uagent.search(svcVodafone) > -1)
      return true;
   if (uagent.search(manuSamsung1) > -1 ||
	uagent.search(manuSonyEricsson) > -1 || 
	uagent.search(manuericsson) > -1)
   {
      return true;
   }
   return false;
};
function RedirectIfMobile(url)
{
   if (DetectMobileLonger())
      {
           window.location = url;
      }
};
RedirectIfMobile('http://www.pearl-harbor.mobi/');