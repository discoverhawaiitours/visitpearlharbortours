<?php include("cookie.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Visit Pearl Harbor All Day Activities Pass, Honolulu Historic Tours </title>
<meta name="Description" content="This Honolulu activities pass includes a visit to Pearl Harbor and its top attractions: the USS Arizona Memorial, USS Bowfin Submarine, USS Missouri Battleship, and Pacific Aviation Museum. "/>
<meta name="Keywords" content="pearl harbor, visit, tour, oahu, honolulu, arizona memorial, uss Missouri battleship,  pacific aviation museum, bowfin submarine, punchbowl, downtown, hawaii, history, attack, iolani palace, king kamehameha statue,"/>
<META NAME="robots" CONTENT="INDEX,FOLLOW"> 
<META NAME="robots" CONTENT="noarchive"> 
<META NAME="audience" CONTENT="all"> 
<link href="VHPT.css" rel="stylesheet" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="RSS Feed for visitpearlharbortours.com" href="http://www.visitpearlharbortours.com/visit.xml">
<script type="text/javascript" src="js/contentslider.js"></script>
<link rel="stylesheet" type="text/css" href="style/pro_pg_contentslider.css" />

<link href="http://www.visitpearlharbortours.com/titty.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="http://www.visitpearlharbortours.com/javascript.js"></script>
<?php include("functions.php"); ?>

</head>
<body>
<table width="909" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="532" rowspan="2"><a href="index.htm"><img src="images/Logo.png" width="532" height="90" border="0" /></a></td>
    <td width="377" height="38" align="center" class="Buttons"><a href="index.htm">Home</a>&nbsp;  |&nbsp;  <a href="oahutour.htm">Tour</a>&nbsp;  |&nbsp;  <a href="photos.htm">Photos</a>&nbsp;  |&nbsp;  <a href="about.htm">About</a>&nbsp;   |&nbsp;  <a href="contact.htm">Contact</a></td>
  </tr>
  <tr>
    <td align="center"><a name="trustlink" href="http://secure.trust-guard.com/certificates/www.visitpearlharbortours.com" target="_blank"
onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace('http', 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;">
<img name="trust seal" alt="Security Verified" style="border: 0;" src="https://secure.trust-guard.com/seals/4694/security/header/gray" /></a><script type="text/javascript" src="https://secure.trust-guard.com/seals/seal-scripts/4694.js"></script></td>
</td>
  </tr>
  <tr>
    <td colspan="2"><table width="909" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/ContentTop.png" alt="a Day at Pearl Harbor" width="909" height="18" /></td>
      </tr>
      <tr>
        <td align="center" valign="top" background="images/ContentBG.png"><table width="855" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2">
            
            <div id="slider2" class="sliderwrapper">
  <div class="contentdiv" style="background-image:url(rotors/1.JPG);"> </div>
  <div class="contentdiv" style="background-image:url(rotors/2.JPG);"> </div>
  <div class="contentdiv" style="background-image:url(rotors/3.JPG);"> </div>
  <div class="contentdiv" style="background-image:url(rotors/4.JPG);"> </div>
  <div class="contentdiv" style="background-image:url(rotors/5.JPG);"> </div>
</div>
<div id="paginate-slider2" > </div>
<script type="text/javascript">
featuredcontentslider.init({

	id: "slider2",  //id of main slider DIV

	contentsource: ["inline", ""],  //Valid values: ["inline", ""] or ["ajax", "path_to_file"]

	toc: "markup",  //Valid values: "#increment", "markup", ["label1", "label2", etc]

	nextprev: ["Previous", "Next"],  //labels for "prev" and "next" links. Set to "" to hide.

	revealtype: "click", //Behavior of pagination links to reveal the slides: "click" or "mouseover"

	enablefade: [true, 0.1],  //[true/false, fadedegree]

	autorotate: [true, 4000],  //[true/false, pausetime]

	onChange: function(previndex, curindex){  //event handler fired whenever script changes slide

		//previndex holds index of last slide viewed b4 current (1=1st slide, 2nd=2nd etc)

		//curindex holds index of currently shown slide (1=1st slide, 2nd=2nd etc)

	}

})



</script>
</div>
</div>
            </td>
          </tr>
          <tr>
            <td width="545"><img src="images/OahuTitle.GIF" width="545" height="32" /></td>
            <td width="340"><img src="images/NotStayingTitle.GIF" width="340" height="32" /></td>
          </tr>
          <tr>
            <td align="left" background="images/H1BG.GIF" class="Buttons"><h1><br />
&nbsp;              &nbsp;&nbsp;&nbsp;Complete Pearl Harbor Tour ~ From Oahu</h1></td>
            <td><img src="images/NotStayOahu.GIF" width="340" height="67" border="0" usemap="#Map" /></td>
          </tr>
          <tr>
            <td align="center" valign="top"><br />
              <table width="466" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="209"><table width="202" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="22" height="25"><img src="images/Star.png" width="18" height="17" /></td>
                    <td width="180" height="25" align="left"><span class="content">USS Arizona Memorial</span></td>
                  </tr>
                  <tr>
                    <td height="25"><img src="images/Star.png" width="18" height="17" /></td>
                    <td height="25" align="left"><span class="content">USS Bowfin Submarine</span></td>
                  </tr>
                  <tr>
                    <td height="25"><img src="images/Star.png" width="18" height="17" /></td>
                    <td height="25" align="left"><span class="content">USS Missouri Battleship</span></td>
                  </tr>
                  <tr>
                    <td height="25"><img src="images/Star.png" width="18" height="17" /></td>
                    <td height="25" align="left"><span class="content">Pacific Aviation Museum</span></td>
                  </tr>
                  <tr>
                    <td height="25"><img src="images/Star.png" width="18" height="17" /></td>
                    <td height="25" align="left"><span class="content">Historic Honolulu Tour</span></td>
                  </tr>
                </table></td>
                <td width="257"><table width="250" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="22" height="25"><img src="images/Star.png" width="18" height="17" /></td>
                    <td width="228" height="25" align="left"><span class="content">Live Historic Naration</span></td>
                  </tr>
                  <tr>
                    <td height="25"><img src="images/Star.png" width="18" height="17" /></td>
                    <td height="25" align="left"><span class="content">Hotel Pickup/Dropoff</span></td>
                  </tr>
                  <tr>
                    <td height="25"><img src="images/Star.png" width="18" height="17" /></td>
                    <td height="25" align="left"><span class="content">Air Conditioned Mini Coach</span></td>
                  </tr>
                  <tr>
                    <td height="25"><img src="images/Star.png" width="18" height="17" /></td>
                    <td height="25" align="left"><span class="content">No-Host Lunch (Blue Hawaian Cafe)</span></td>
                  </tr>
                  <tr>
                    <td height="25"><img src="images/Star.png" width="18" height="17" /></td>
                    <td height="25" align="left"><span class="content">See Pacific Fleet Headquarters</span></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
            <td><table width="335" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="335"><img src="images/PriceTop.GIF" width="340" height="24" /></td>
              </tr>
              <tr>
                <td align="center" valign="top" background="images/PriceBG.GIF">
                
                
                
                
                
                <?php
				//---------------------------------start calendar!!!!!!!!! ------------------------------------//
				?>
                
                
                
                
                <div id="rightcolumn">

<table width="300" border="0">
  <tr>
    <td colspan="2"><span  class="rightheader">BOOK A TOUR // CHOOSE A DATE</span></td>
    </tr>
    
  
  <tr>
    <td colspan="2" align="center">
    
    <?php
	
	

	//-----------------------------CHANGE FOR EACH TOUR----------------------------------//
	
	$priceadult = 99.99;
	$pricechild = 89.99;
	$tourname = "A Day At Pearl Harbor Tour";
	$tourid = "2A";
	$urlname = "oahutour";
	
	//----------------------------------------------------------------------------------//
 
	
	
	
	
    
    
//START CALENDAR
if (($_GET['month'] != '') && ($_GET['year'] != '')) {

$month = htmlentities($_GET['month']);
$year = htmlentities($_GET['year']);

} else {

//This gets today's date
$date =time () ;

//This puts the day, month, and year in seperate variables
$day = date('d', $date) ;
$month = date('m', $date) ;
$year = date('Y', $date) ;

}


//set cell size
$size = 30;



//Set rangecount for blackout date range
$rangecount = 1;


//Make Next Month 
$nextmonth = $month + 1;
$nextyear = $year;
if ($nextmonth > 12) {
	$nextmonth = 1;
	$nextyear = $year + 1;
}
if (strlen($nextmonth) == 1) {
	$nextmonth = "0".$nextmonth;
}

//Make Previous Month 
$lastmonth = $month - 1;
$lastyear = $year;
if ($lastmonth < 1) {
	$lastmonth = 12;
	$lastyear = $year - 1;
}
if (strlen($lastmonth) == 1) {
	$lastmonth = "0".$lastmonth;
}




//Here we generate the first day of the month
$first_day = mktime(0,0,0,$month, 1, $year) ;

//This gets us the month name
$title = date('F', $first_day) ; 

 //Here we find out what day of the week the first day of the month falls on 
 $day_of_week = date('D', $first_day) ; 



 //Once we know what day of the week it falls on, we know how many blank days occure before it. If the first day of the week is a Sunday then it would be zero

switch($day_of_week){ 
case "Sun": $blank = 0; break; 
case "Mon": $blank = 1; break; 
case "Tue": $blank = 2; break; 
case "Wed": $blank = 3; break; 
case "Thu": $blank = 4; break; 
case "Fri": $blank = 5; break; 
case "Sat": $blank = 6; break; 
}



 //We then determine how many days are in the current month

 $days_in_month = cal_days_in_month(0, $month, $year) ; 
 
 //Here we start building the table heads 
echo '<div id="calbox">';
echo '<div id="calborder">';
 echo "<table border=\"0\" padding=\"0\">";

 echo "<tr><td align=\"center\"><a href=\"http://www.visitpearlharbortours.com/tour/".str_replace(' ', '-', strtolower($tourname))."_".$lastmonth."_".$lastyear.".html\" class=\"chevs\" rel=\"nofollow\">&lt;</a></td><td  align=\"center\" colspan=5>";
 ?>
 
    <select name="select" onChange="MM_jumpMenu('parent',this,0)" id="monthboxes">
    
    <?php

	foreach($months as $key => $value) {
	
		if ($value == $title) {
			echo "<option value=\"http://www.visitpearlharbortours.com/tour/".$urlname."_".$key."_".$year.".html\" selected>$value</option>\n";
		} else {
			echo "<option value=\"http://www.visitpearlharbortours.com/tour/".$urlname."_".$key."_".$year.".html\">$value</option>\n";
		}
	
	}
	echo "</select>";

 ?>
	
    <select name="select" onChange="MM_jumpMenu('parent',this,0)" id="monthboxes">
    
    <?php

	foreach($years as $key => $value) {
	
		if ($value == $year) {
			echo "<option value=\"http://www.visitpearlharbortours.com/tour/".$urlname."_".$month."_".$value.".html\" selected>$value</option>\n";
		} else {
			echo "<option value=\"http://www.visitpearlharbortours.com/tour/".$urlname."_".$month."_".$value.".html\">$value</option>\n";
		}
	
	}
	echo "</select>";

 ?>
 
 
 <?php
 
 
 echo "</td><td align=\"center\"><a href=\"http://www.visitpearlharbortours.com/tour/".str_replace(' ', '-', strtolower($tourname))."_".$nextmonth."_".$nextyear.".html\"  class=\"chevs\" rel=\"nofollow\">&gt;</a></td></tr>";

 echo "<tr><td width=\"$size\" align=\"center\" height=\"$size\" class=\"olddays\">S</td><td width=\"$size\" align=\"center\" height=\"$size\" class=\"olddays\">M</td><td 
width=\"$size\" align=\"center\" height=\"$size\" class=\"olddays\">T</td><td width=\"$size\" align=\"center\" height=\"$size\" class=\"olddays\">W</td><td width=\"$size\" align=\"center\" height=\"$size\" class=\"olddays\">T</td><td width=\"$size\" align=\"center\" height=\"$size\" class=\"olddays\">F</td><td width=\"$siz\"e align=\"center\" height=\"$size\" class=\"olddays\">S</td></tr>";



 //This counts the days in the week, up to 7

 $day_count = 1;



 echo "<tr>";

 //first we take care of those blank days

 while ( $blank > 0 ) 

 { 

 echo "<td></td>"; 

 $blank = $blank-1; 

 $day_count++;

 } 
 
  //sets the first day of the month to 1 

 $day_num = 1;



 //count up the days, untill we've done all of them in the month

 while ( $day_num <= $days_in_month ) { 



		//Add leading zero to day
		if (strlen($day_num) == 1) {
			$day = "0".$day_num;
		} else {
			$day = $day_num;
		}


		//Reformat the dates for comparison
		$current = $year."-".$month."-".$day;
		$today = date('Y-m-d');
	
		
		
		
		
		
		
		if ($rangecount == 1) {
			
			
			//HANDLE BLACKOUTS AND OVERRIDES
			
			//Blackout day of month from DB
			
			$result = mysql_query("SELECT * FROM `rules` WHERE `tourname` = '$tourname' && `type` = 'day'");
			while($row = mysql_fetch_array($result)) {
				$bo[] = $row['date1'];
			}
			//BLACKOUT SPECIFIC DATES
			$result = mysql_query("SELECT * FROM `rules` WHERE `tourname` = '$tourname' && `type` = 'single'");
			while($row = mysql_fetch_array($result)) {
				$bodate[] = $row['date1'];
			}
			//BLACKOUT Date Range
			$result = mysql_query("SELECT * FROM `rules` WHERE `tourname` = '$tourname' && `type` = 'range'");
			while($row = mysql_fetch_array($result)) {
				$start = $row['date1'];
				$finish = $row['date2'];
			}
			//Override Blackout Date
			$result = mysql_query("SELECT * FROM `rules` WHERE `tourname` = '$tourname' && `type` = 'override'");
			while($row = mysql_fetch_array($result)) {
				$override[] = $row['date1'];
			}
			//Sold Out
			$result = mysql_query("SELECT * FROM `rules` WHERE `tourname` = '$tourname' && `type` = 'soldout'");
			while($row = mysql_fetch_array($result)) {
				$soldout[] = $row['date1'];
			}
			//Limited
			$result = mysql_query("SELECT * FROM `rules` WHERE `tourname` = '$tourname' && `type` = 'limited'");
			while($row = mysql_fetch_array($result)) {
				$limited[] = $row['date1'];
			}
			
			
			
			//function to get date range in correct format. 
			$count = 1;
				
				while ($start < $finish) {
				
					if ($count == 1) {
						//echo date('Y-m-d', strtotime($start))."<br />";
						$start = date('Y-m-d', strtotime($start));
					} else {
						//echo date('Y-m-d', strtotime($start."+1 day"))."<br />";
						$start = date('Y-m-d', strtotime($start."+1 day"));
					}
					
					$count = 0;
					$range[] = $start;
			}
			//Set rangecount so it doesnt repeat
			$rangecount = 0;
		}
		
		//Output individual dates
		if ( (!in_array($day_count, $bo)) && ($today <= $current) && (!in_array($current, $bodate)) && (!in_array($current, $range)) && (!in_array($current, $soldout)) ) {
		
		
			//check if date is limited 
			if (in_array($current, $limited)) {
			
			echo "<td bgcolor=\"#FFFF00\" align=\"center\" height=\"$size\" width=\"$size\"><a href=\"#\" onclick=\"ElementContent('date','$month / $day / $year')\" class=\"days\"> $day_num </a></td>\n"; 
						
			} else {
		
			echo "<td bgcolor=\"#00CC00\" align=\"center\" height=\"$size\" width=\"$size\"><a href=\"#\" onclick=\"ElementContent('date','$month / $day / $year')\" class=\"days\"> $day_num </a></td>\n"; 
			 
			 }
			 
			 
		} else {
			//override blackout dates
			if (in_array($current, $override) && ($today <= $current)) {
			
				echo "<td bgcolor=\"#00CC00\" align=\"center\" height=\"$size\" width=\"$size\"><a href=\"#\" onclick=\"ElementContent('date','$month / $day / $year')\" class=\"days\"> $day_num </a></td>\n";
				
			} else {
				
				//check if date is sold out
				if (in_array($current, $soldout) && ($today <= $current)) {
					echo "<td bgcolor=\"#FF6600\" align=\"center\" height=\"$size\" width=\"$size\" class=\"olddays\"> $day_num </td>\n"; 
				} else {
					echo "<td bgcolor=\"#999999\" align=\"center\" height=\"$size\" width=\"$size\" class=\"olddays\"> $day_num </td>\n"; 
				}
			
				
			
			}
		}
		

 $day_num++; 

 $day_count++;



 //Make sure we start a new row every week

 if ($day_count > 7)

 {

 echo "</tr><tr>";

 $day_count = 1;

 }

 } 
 
  //Finaly we finish out the table with some blank details if needed

 while ( $day_count >1 && $day_count <=7 ) 

 { 

 echo "<td> </td>"; 

 $day_count++; 

 } 

 
 echo "</tr></table></div>"; 
 

 
 
 ?>

 <div id="legend">
 <table border="0" cellpadding="0" cellspacing="3" class="smallcal">
  <tr>
    <td width="15" height="15" bgcolor="#00CC00"></td>
    <td height="15">Available</td>
    <td width="15" height="15" bgcolor="#FFFF00"></td>
    <td height="15">Limited</td>
    <td width="15" height="15" bgcolor="#999999"></td>
    <td height="15">Not Available</td>
    <td width="15" height="15" bgcolor="#FF6600"></td>
    <td height="15">Sold Out</td>
  </tr>
</table>
 </div>
 <form name="form1" method="post" action="http://www.visitpearlharbortours.com/cart" onSubmit="return validate_form(this)">
 
 
    <table width="300" border="0" cellspacing="4">
     
      <tr>
        <td align="right"><span class="smallcal">Date:</span>
          <input name="tourdate" type="text" id="date" size="12" readonly="readonly" onsubmit="return validate_form(this)" class="calboxes" /></td><td>&nbsp;</td></tr>
        <tr><td align="right"><span class="calprice"><?php echo "$".$priceadult; ?></span> <span class="smallcal">per Adult x:</span>
           <input name="adults" type="text" id="date"  class="calboxes" size="2" value="1" />
          </td><td width="25">&nbsp;</td></tr>
        <tr><td align="right"><span class="calprice"><?php echo "$".$pricechild; ?></span> <span class="smallcal">per Child x:</span>
          <input name="children" type="text" id="date"  class="calboxes" size="2" value="0" />
          </td><td>&nbsp;</td>
      </tr>
          
          <?php
		  
		  	if ($cancelprotect == 1) {
				
				?>
                
                    <tr><td align="right" width="120"><span class="calprice">Cancellation Protection: <a href="#" onClick="pop('http://www.visitpearlharbortours.com/pops.php?id=cancel')">(?)</a></span><input name="cancellationprotection" type="checkbox" value="1" />
                    </td><td>&nbsp;</td>
                    </tr>
                
                
                <?php
				
				
				
			}
		  
		  ?>
          <?php
		  
		  	if ($mysteryshopper == 1) {
				
				?>
                
                    <tr><td align="right" width="120"><span class="calprice">Mystery Shopper: <a href="#" onClick="pop('http://www.visitpearlharbortours.com/pops.php?id=mystery')">(?)</a></span><input name="mysteryshopper" type="checkbox" value="1" />
                    </td><td>&nbsp;</td>
                    </tr>
                
                
                <?php
				
				
				
			}
		  
		  ?>
          <?php
		  
		  	if ($airportpickup == 1) {
				
				?>
                
                    <tr><td align="right" width="120"><span class="calprice">Airport Pickup: <a href="#" onClick="pop('http://www.visitpearlharbortours.com/pops.php?id=airport')">(?)</a></span><input name="airportpickup" type="checkbox" value="1" />
                    </td><td>&nbsp;</td>
                    </tr>
                
                
                <?php
				
				
				
			}
		  
		  ?>
          <?php
		  	/*
			if ($dropdown1 != '') {
			
				echo "<tr><td align=\"right\" width=\"120\">";
				
				$pee = explode(', ', $dropdown1);
				
				echo "<span class=\"calprice\">".$pee[0]."</span>: <select name=\"".$pee[0]."\"><option value=\"\">Choose</option>";
				
				foreach($pee as $key => $value) {
					if ($key != 0) {
						echo "<option value=\"$value\">$value</option>\n";
					
					}
				
				}
				echo "</select></td><td>&nbsp;</td></tr>";
			
			
			}
			
		  
		  ?>
          <?php
		  	
			if ($dropdown2 != '') {
			
				echo "<tr><td align=\"right\" width=\"120\">";
				
				$pee = explode(', ', $dropdown2);
				
				echo "<span class=\"calprice\">".$pee[0]."</span>: <select name=\"".$pee[0]."\"><option value=\"\">Choose</option>";
				
				foreach($pee as $key => $value) {
					if ($key != 0) {
						echo "<option value=\"$value\">$value</option>\n";
					
					}
				
				}
				echo "</select></td><td>&nbsp;</td></tr>";
			
			
			}
			*/
		  
		  ?>
          
      </td>
    <td align="right">
    <input type="hidden" name="sadult" value="<?php echo $priceadult; ?>">
    <input type="hidden" name="schild" value="<?php echo $pricechild; ?>">
  <input type="hidden" name="staticname" value="<?php echo $tourname; ?>">
  <input type="hidden" name="statictourcode" value="<?php echo $tourid; ?>">
  <input type="hidden" name="staticdepart" value="A">
  <input type="submit" name="submit" id="checkoutbut" value="BOOK NOW">

</td>
  </tr></table></td></tr></table>


</form>
                
                
                
                
                
                
                
                
                
                
                
                </td>
              </tr>
              <tr>
                <td><img src="images/PriceBottom.GIF" width="340" height="18" /></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td colspan="2" align="left"><blockquote>
              <p class="content">Pearl Harbor tour featuring USS Arizona Memorial, USS Missouri Battleship, Pacific Aviation Museum, USS Bowfin Submarine, USS Oklahoma Memorial, and Historic Honolulu Tour.</p>
              <p class="content">Experience Hawaii’s war stricken history on this full day tour of Pearl Harbor with complimentary hotel pick-up from Waikiki in a deluxe air-conditioned 24 passenger Mini Coach.  En-route to Pearl Harbor your expert tour guide will share with you the fascinating history behind the Japanese attack on the United States of America.</p><table width="757" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="757"><img src="images/GridTop.GIF" width="780" height="30" /></td>
  </tr>
  <tr>
    <td height="41" align="center" background="images/GridBG.GIF"><table width="771" border="0" cellspacing="5" cellpadding="0">
      <tr>
        <td width="132" align="left" valign="top" class="content2">Tour operates daily</td>
        <td width="129" align="left" valign="top" class="content2">6:00am</td>
        <td width="134" align="left" valign="top" class="content2">4:45pm</td>
        <td width="142" align="left" valign="top" class="content2">Covered Shoes<br />
          Comfortable Clothes<br />
          No Swim Suits<br />
          Sunscreen<br />
          Shirts Required</td>
        <td width="204" align="left" valign="top" class="content2">Proffessional tour guide<br />
          Admission To All Exhibts<br />
          Roundtrip Hotel Transportation<br />
          Historic Narration<br />
          Air-Conditioned Mini Bus</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="12"><img src="images/GridBottom.GIF" width="780" height="12" /></td>
  </tr>
</table>
&nbsp;</p>
              <p class="content">Upon arriving to Pearl Harbor your professional tour guide will provide you with information about the attack on Pearl Harbor and Oahu.  Then, you will visit the Arizona Memorial via the US Navy shuttle boat.  Witness the “Black Tears of the Arizona" as you stand over the sunken battleship where 1,177 men still lie to rest.</p>
              <p class="content">After a short drive over to Ford Island the next stop is a professionally guided tour onboard the USS Missouri Battleship, or “Might Mo."  Explore the many different decks while learning about the Japanese Surrender which ended WWII.  Other visits at Pearl Harbor include a guided tour of the Pacific Aviation Museum as well as the USS Bowfin Submarine. </p>
              <p class="content">Enjoy learning about Hawaiian culture and history with a historic tour of downtown Honolulu and surrounding area.  View the Iolani Palace, King Kamehameha Statue, Punchbowl National Cemetery, and much more as your professional guide drives you through Honolulu. </p>
              <p class="content">This Pearl Harbor and War Memorial Tour is a must see for anyone visiting Hawaii who enjoys history and learning about WWII in the Pacific. </p>
              <p><font color="#990000" size="2">USS Arizona Memorial Closure</font></p>
              <p><font color="#990000" size="2"><strong>February 10, 2011</strong> - Due to necessary construction to the USS Arizona Memorial, visitors will not be permitted to board the structure throughout the course of the day. In place of boarding the Memorial, the National Parks Service will operate the boat only portion of the tour to an area near the Memorial, where a NPS Park Ranger will discuss the significance of Battleship Row, the meaning of the USS Arizona Memorial, and the Pearl Harbor Historic Sites.  The USS Bowfin Submarine, Battleship Missouri, and the Pacific Aviation Museum will remain open, as will the new Visitor Center Museum.</font>              </p>
              <p class="content">&nbsp;</p>
              <p class="content">&nbsp;</p>
            </blockquote>              </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center" valign="top"><img src="images/ContentBottom.png" width="909" height="28" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><a href="about.htm"><br /></a><a href="about.htm"><span class="Buttons">About Us</span></a><span class="Buttons"> | <a href="faq.htm">FAQ</a> | <a href="http://www.visitpearlharbortours.com/reviews.html">Reviews</a> | <a href="contact.htm">Contact Us</a> | <a href="terms.htm">Terms</a> | <a href="privacy.htm">Privacy Policy</a> | <a href="cancellations.htm">Cancellations</a> | <a href="sitemap.html">HTML Sitemap</a></span> | <span class="Buttons"><a href="http://www.visitpearlharbortours.com/blog/">Blog</a></span><br />
    <br /></td>
  </tr>
</table>

<map name="Map" id="Map">
  <area shape="rect" coords="2,2,101,62" href="bigislandtour.htm" />
  <area shape="rect" coords="110,3,207,62" href="mauitour.htm" />
  <area shape="rect" coords="222,2,323,62" href="kauaitour.htm" />
</map>
</body>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3498552-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


</script>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>

</html>
