<?php require "config.php"; 

/**
 * @author SMARTREVIEWSCRIPT.COM
 * @copyright 2010
 */

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Visit Pearl Harbor Tours Reviews</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<META NAME="robots" CONTENT="INDEX,FOLLOW"> 
<META NAME="robots" CONTENT="noarchive"> 
<META NAME="audience" CONTENT="all"> 
<link href="../VHPT.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="909" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="532" rowspan="2"><a href="../index.htm"><img src="../images/Logo.png" width="532" height="90" border="0" /></a></td>
    <td width="377" height="38" align="center" class="Buttons"><a href="../index.htm">Home</a>&nbsp;  |&nbsp;  <a href="../oahutour.htm">Tour</a>&nbsp;  |&nbsp;  <a href="../photos.htm">Photos</a>&nbsp;  |&nbsp;  <a href="../about.htm">About</a>&nbsp;   |&nbsp;  <a href="../contact.htm">Contact</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><table width="909" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="../images/HomeTop.png" width="909" height="199" /></td>
      </tr>
      <tr>
        <td align="center" valign="top" background="../images/ContentBG.png"><table width="864" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="864"><img src="../images/RezgoTitlebar.GIF" width="868" height="27" /></td>
          </tr>
          <tr>
            <td align="left"><blockquote> 
              <h1><br />
                <a href="submit.php">Submit A new Review</a></h1>
              <p class="content"><?php 

/** 
	PLEASE MAKE SURE YOU READ THE README.TXT FILE FIRST!!!
**/

// Insert a link to your main page ***MODIFY THIS***
$mainpage = "http://www.visitpearlharbortours.com";

// If php is successful in receiving your sku from index.php?sku=YOUR_SKU_ID_HERE  
if(!empty($_GET['sku']))
{
	// Stores the product sku as a session
	$_SESSION['sku'] = $_GET['sku'];
	
	echo "<table width='800 border='0' cellpadding='0' cellspacing='0' >";
	echo "<tr>";
	echo "<td colspan='2'>"; 
	echo "</td>";
	echo "</tr>";	
	echo "<tr>";
	echo "</tr>";
	echo "<tr class='even'>";
	echo "<td colspan='2'><div align='center'><h1>Customer Reviews</h1>";
	$result = mysql_query("SELECT * FROM $tablecomments WHERE sku ='".$_SESSION['sku']."' ORDER BY id DESC") or die(mysql_error());
	$numberofrows = mysql_num_rows($result);
	if($numberofrows != 0)
	{
		echo "<br><p class='style3'>Average rating: ";
		while($row = mysql_fetch_array($result))
		{
			$rating += $row[rating];
			$numberofrows;
		}
		$avgrating = round($rating/$numberofrows, 2);
		echo $avgrating." / 5<br>Total reviews: ".$numberofrows;
	}
	
	
	echo "</p></div></td>";
	echo "</tr>";
	echo "<tr><td colspan='2' class='style3'>";
	echo "</td></tr>";
		
	$abc123 = mysql_query("SELECT * FROM $tablecomments WHERE sku ='".$_SESSION['sku']."' ORDER BY id DESC") or die(mysql_error());
	while($row = mysql_fetch_array($abc123))
	{
		// Figures out which star rating image to print
		if($row[rating] == 5)
			$rating = "<img src='images/star5.gif' alt='5'>";
		elseif($row[rating] == 4)
			$rating = "<img src='images/star4.gif' alt='4'>";
		elseif($row[rating] == 3)
			$rating = "<img src='images/star3.gif' alt='3'>";
		elseif($row[rating] == 2)
			$rating = "<img src='images/star2.gif' alt='2'>";
		elseif($row[rating] == 1)
			$rating = "<img src='images/star1.gif' alt='1'>";						 
		elseif($row[rating] == 0)
			$rating = "<img src='images/star0.gif' alt='0'>";		
		
		
		// Alternates row colors
		$i;
		if($i%2 != 0)
		{
			$class = "class='even'";
		}
		else
		{
			$class = "class='odd'";
		}
		$i++;
		
		// We first retrieve the time thats in unix mysql format then we create it as variable $timeCreate and reformat with function date_format
		$timeSRC = $row['time'];
		$timeCreate = date_create($timeSRC);
		
		echo "<tr $class>";
	    echo "<td width='446' class='style1'>".$rating." ".$row['title']." by ".$row['name']."</td>";
	    echo "<td width='144' class='style1'><div align='right'>".date_format($timeCreate, 'n/j/Y g:i:s A')."</div></td>";
	  	echo "</tr>";
	  	echo "<tr $class>";
	  	echo "<td colspan='2'>";
	  	
	  	// Prints pros, cons, and other thoughts fields
	  	if($row['comment'])
	  	{
	    	echo "<p class='style2'>".$row['comment']."</p><br>";
	    }
	  	echo "</tr>";
	  	echo "<tr $class>";
	  	$id = $row['id'];
	  	echo "</tr>";
	}
	
	
	echo "</table>";
}
else
{
	echo "<p class='red'>Error</p>";
	echo "<p>The sku value is not specified in the address link. Please refer to the README.TXT file.</p>";
}
?></li>
              </ul>
            </blockquote></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td width="910" height="255" background="../images/HomeBottomBG.png">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><a href="../about.htm"><br />
      <span class="Buttons">About Us</span></a><span class="Buttons"> | <a href="../faq.htm">FAQ</a> | <a href="http://www.visitpearlharbortours.com/reviews/index.php?sku=reviews">Reviews</a> | <a href="../contact.htm">Contact Us</a> | <a href="../terms.htm">Terms</a> | <a href="../privacy.htm">Privacy Policy</a> | <a href="../cancellations.htm">Cancellations</a> | <a href="../sitemap.html">HTML Sitemap</a></span><br />
    <br /></td>
  </tr>
</table>
</body>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3498552-29");
pageTracker._setDomainName("none");
pageTracker._setAllowLinker(true); 
pageTracker._initData();
pageTracker._trackPageview();
</script>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</html>
