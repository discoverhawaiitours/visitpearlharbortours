<?php require "config.php"; 

/**
 * @author SMARTREVIEWSCRIPT.COM
 * @copyright 2010
 */

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Visit Pearl Harbor Tours Reviews</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<META NAME="robots" CONTENT="INDEX,FOLLOW"> 
<META NAME="robots" CONTENT="noarchive"> 
<META NAME="audience" CONTENT="all"> 
<link href="../VHPT.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="909" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="532" rowspan="2"><a href="../index.htm"><img src="../images/Logo.png" width="532" height="90" border="0" /></a></td>
    <td width="377" height="38" align="center" class="Buttons"><a href="../index.htm">Home</a>&nbsp;  |&nbsp;  <a href="../oahutour.htm">Tour</a>&nbsp;  |&nbsp;  <a href="../photos.htm">Photos</a>&nbsp;  |&nbsp;  <a href="../about.htm">About</a>&nbsp;   |&nbsp;  <a href="../contact.htm">Contact</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><table width="909" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="../images/HomeTop.png" width="909" height="199" /></td>
      </tr>
      <tr>
        <td align="center" valign="top" background="../images/ContentBG.png"><table width="864" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="864"><img src="../images/RezgoTitlebar.GIF" width="868" height="27" /></td>
          </tr>
          <tr>
            <td align="left"><blockquote> 
            <br />
              <p class="content">
              <FORM action="reviewthanks.php" method="post" name="form1" id="form1" style="margin:0px; font-family:Verdana, Arial, Helvetica, sans-serif;font-size:11px; width:300px;" onSubmit="MM_validateForm('from','','RisEmail','verif_box','','R','message','','R', 'name','','R', 'star','','R');return document.MM_returnValue">
<P>Your E-mail:<BR />
<INPUT name="from" type="text" id="from" style="padding:2px; border:1px solid #CCCCCC; width:180px; height:24px; font-family:Verdana, Arial, Helvetica, sans-serif;font-size:11px;" value=""/>
<BR />

<BR />
Your Name:<BR />
<INPUT name="name" type="text" id="name" style="padding:2px; border:1px solid #CCCCCC; width:180px; height:24px; font-family:Verdana, Arial, Helvetica, sans-serif;font-size:11px;" value=""/>

<BR />
<BR />

Your Rating (from 1 to 5):<BR />
<INPUT name="star" type="text" id="star" style="padding:2px; border:1px solid #CCCCCC; width:180px; height:24px; font-family:Verdana, Arial, Helvetica, sans-serif;font-size:11px;" value=""/>

<BR />
<BR />

Type verification image:<BR />
<INPUT name="verif_box" type="text" id="verif_box" style="padding:2px; border:1px solid #CCCCCC; width:180px; height:24px;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;"/>
<IMG src="verificationimage.php?890" alt="verification image, type it in the box" width="50" height="24" align="absbottom" /><BR />
<BR />
<!-- if the variable "wrong_code" is sent from previous page then display the error field -->
Message:<BR />
<TEXTAREA name="message" cols="6" rows="5" id="message" style="padding:2px; border:1px solid #CCCCCC; width:300px; height:100px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;"></TEXTAREA>
<INPUT name="Submit" type="submit" style="margin-top:10px; display:block; border:1px solid #000000; width:100px; height:20px;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; padding-left:2px; padding-right:2px; padding-top:0px; padding-bottom:2px; line-height:14px; background-color:#EFEFEF;" value="Submit Review"/>

</FORM>
              </li>
              </ul>
            </blockquote></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td width="910" height="255" background="../images/HomeBottomBG.png">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><a href="../about.htm"><br />
      <span class="Buttons">About Us</span></a><span class="Buttons"> | <a href="../faq.htm">FAQ</a> | <a href="http://www.visitpearlharbortours.com/reviews/index.php?sku=reviews">Reviews</a> | <a href="../contact.htm">Contact Us</a> | <a href="../terms.htm">Terms</a> | <a href="../privacy.htm">Privacy Policy</a> | <a href="../cancellations.htm">Cancellations</a> | <a href="../sitemap.html">HTML Sitemap</a></span><br />
    <br /></td>
  </tr>
</table>
</body>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3498552-29");
pageTracker._setDomainName("none");
pageTracker._setAllowLinker(true); 
pageTracker._initData();
pageTracker._trackPageview();
</script>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</html>
