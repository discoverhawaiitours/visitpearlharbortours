PLEASE READ THE FOLLOWING TO INSURE YOUR SCRIPT WORKS CORRECTLY:

1. Make sure your php and mysql is up to date.

2. Create a mysql database.

3. Then check the config.php file and make sure to configure the settings accordingly (lines 13-16). 
   Warning: Make sure these settings are configured correctly. If the settings are incorrect the review
   script will not be able to connect to your database and the script won't work.

   Also modify the main page link in the files index.php (ln 26). There should be a ***MODIFY THIS*** 
   notification in the comments.

4. Make sure to have a special link for each of your product pages. Add ?sku= after your index.php and 
   specify a different item number (not longer than 32 characters) for each product.
   (i.e. http://www.buy.com/products/index.php?sku=ABC01)
  
   What this does is that it specifies what product the reviews on this page are associated with.

   You can do this by simply changing your html links to something like:
   (i.e. <a href="http://www.buy.com/products/index.php?sku=ABC01">Read Reviews</a>


5. Lastly, if you would like to change your timezone, open the add.php file
   and go to line 29 --> date_default_timezone_set("MST"). Just simply type
   in the timezone you would like to use for your reviews between the double 
   quotes and save the file. Go to http://php.net/manual/en/timezones.php to 
   find a list of supported timezones.


Note: You don't need to manually create any mysql tables. This script was specifically designed 
      to recognizes whether the required tables exist or not, so that it may create them accordingly.
      
      If you like this free script and would like to see more features visit us at smartreviewscript.com
      and if you have any questions please dont hesitate to contact us at support@smartreviewscript.com