<?php require "config.php"; 

/**
 * @author SMARTREVIEWSCRIPT.COM
 * @copyright 2010
 */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="author" content="SMARTREVIEWSCRIPT.COM" />
<title>Add Your Review</title>
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
<div id='main'>
<?php
$sku = $_SESSION['sku'];
$name = $_POST['name'];
$title = $_POST['reviewtitle'];
$rating = $_POST['rating'];
$comment = $_POST['comment'];

if(!empty($name) && !empty($title) && !empty($rating) && !empty($comment))
{		
		// Sets date and time ***MODIFY THIS***
		date_default_timezone_set("MST"); // Go to http://php.net/manual/en/timezones.php to find a list of supported timezones
		$time = date("Y-m-d H:i:s");
		
		$comment = $_POST['comment'];
		
		// Adds review to the database
		$insert = mysql_query("INSERT INTO $tablecomments(sku, name, title, rating, time, comment) VALUES('".$_SESSION['sku']."', '$name', '$title', '$rating', '$time','$comment')");
							
		if($insert)
		{
			echo "<p class='green'>Success</p>";
        	echo "<p>Your review has successfully been created.</p>";
			echo "<p>Please wait while we redirect you to the main page...</p>";			
		
			// Redirects user to the main page after 5 seconds
			echo "<meta http-equiv='refresh' content='0;url=index.php?sku=".$_SESSION['sku']."' />";
			mysql_close($con);
		}
		else
		{
			echo "<p class='red'>Error</p>";
			echo "<p>Inserting review into mysql database was unsuccessful. <a href='addreview.php?sku=".$_SESSION['sku']."'>Click here to go back and try again.</p>";
		}
}
// Returns error if user hasn't entered the required fields
else
{
	echo "<p class='red'>Error</p>";
	echo "<p>";
	
	if(empty($_POST['name']))
		echo "Name field is empty.<br>";
	
	if(empty($_POST['reviewtitle']))
		echo "Review title field is empty.<br>";
		
	if(empty($_POST['rating']))
		echo "Please rate the product.<br>";
		
	if(empty($_POST['comment']))
		echo "Comment field is empty.<br>";
		
	echo "<a href='addreview.php?sku=".$_SESSION['sku']."'>Click here to go back and try again.</a>";
	echo "</p>";
}

?>
</div>
</body>
</html>