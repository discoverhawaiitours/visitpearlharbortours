<?php
$topic = htmlentities($_GET['id']);
?>
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #333333;
}
body {
	background-color: #999999;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
#popbox {
	width: 350px;
	margin: 15px;
	height: 150px;
	background-color: #FFFFFF;
	color: #333333;
	padding: 10px;
	border: 1px solid #333333;
}
#head {
	font-size: 16px;
	color: CC3300;
	font-weight: bold;
	margin-bottom: 10px;
}
#termbox {
	width: 550px;
	margin: 15px;
	overflow: auto;
	background-color: #FFFFFF;
	color: #333333;
	padding: 10px;
	border: 1px solid #333333;
}
-->
</style>

<?php
if ($topic == "airport") {
	echo "<div id=\"popbox\"><div id=\"head\">Airport Pick-up: $25 Per Person</div>If you are arriving in Honolulu on the morning of your tour, please check this box for round trip airport pick-up fee. Airport Pick-Up is unavailable Friday and Sunday.</div>";
}
if ($topic == "mystery") {
	echo "<div id=\"popbox\"><div id=\"head\">Mystery Shopper</div>Check the box if you would like an opportunity to get this tour for a discounted price by participating as a Mystery Shopper. Also understand that participants are chosen at random and will be contacted by email if chosen. Participants can opt out at any time.</div>";
}
if ($topic == "cancel") {
	echo "<div id=\"popbox\"><div id=\"head\">Cancellation Protection: $25 Per Person</div> Purchasing this cancellation protection allows you to cancel your reservation up to 48 hours prior to departure and receive a full refund minus a 10% processing fee.</div>";
}
?>
</div>
<?php
if ($topic == "terms") {
	echo "<div id=\"termbox\"><div id=\"head\">Terms and Conditions</div>
	<br />
	FARES AND PRICES<br />
	All fares and prices quoted are subject to availability. Prices are subject to change until full payment is received and voucher(s) are issued. Prices that are quoted include all taxes and fees unless we advise otherwise.<br /><br />
	AVAILABILITY<br />
	The travel products and services sold through this site are subject to availability and can be withdrawn without notice. Tours are confirmed when full payment is received and a voucher issued.
	<br /><br />
	YOUR VOUCHER<br />
	This webpage is verification that your reservation has been received. It is not a tour voucher. A detailed voucher with departure point and pick up time will be emailed to you within 48 hours of receiving your reservation.
	<br /><br />
	IMPORTANT<br />
	We request that you carefully check the dates on your voucher and to contact us immediately if these are not correct.
	<br /><br />
	CANCELLATION POLICY<br />
	If your plans change for any reason 48 hours or more prior to a tour that does not include airfare, you may cancel without penalty and receive a 100% refund. Reservations cancelled within 48 hours of the event are non-refundable. Due to airline ticket restrictions, tours and packages that include airfare are non-refundable.
	<br /><br />
	ADDITIONAL INFORMATION<br />
	We do not guarantee the viewing of active lava flow. Often hiking over rough, uneven, lava surfaces is required to view lava. Volcanic fumes (VOG): VOG is hazardous to your health. Persons with breathing and heart difficulties, pregnant women, infants, and young children are especially at risk and should consult a health care professional before booking a volcano tour. Sturdy, covered walking shoes are recommended.
	<br /><br />
	Schedule, prices & features are seasonal, time permitting and subject to change without notice. Discover Hawaii Tours shall not be liable for delays, injury, damage, loss or theft to person(s) or property.
	<br /><br />
	Discover Hawaii Tours is not responsible for and will not extend financial reimbursement for delays, traffic, wildlife, volcanic activity, inclement weather, mechanical problems, and/or shutdown of Navy boats at Pearl Harbor. No bags or purses permitted at Pearl Harbor. Storage is available for $3.00 prior to entry. Discover Hawaii Tours is not responsible for long lines and delays which occur frequently at the National Park and other sites. There have been occasions where the Arizona Memorial runs out of tickets. In such cases, guests on land only tours are given the option to reschedule; guests on air tours may reschedule, but will be charged the cost of airfare.
	<br /><br />
	TRAVEL RECOMMENDATION<br />
	Early morning tours are not recommended for the day after travel due to possible travel delays en route to Hawaii</div>";
}

?>
