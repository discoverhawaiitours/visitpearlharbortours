function BillingDataService($rootScope) {
  
};
discoverModule.factory('billingDataService', ['$rootScope', BillingDataService]);

function BillingDataService($rootScope) {
  	var billing = {};

	billing.define = function() {
	  if(typeof($rootScope.billingData) == "undefined"){
	  	$rootScope.billingData = {}
	  	$rootScope.billingData.agree = true;
	  	$rootScope.billingData.emails = $rootScope.billingData.emails || [{}]; // default to one email that's empty so can repeat on fields
	  	$rootScope.billingData.country = 'United States';
	  }
	};
	return billing;
};
discoverModule.factory('billingDataService', ['$rootScope', BillingDataService]);

function StepUpdateService($rootScope) {
  var stepUpdate = {};

  stepUpdate.message = '';

  stepUpdate.updateStep = function(step) {
    this.step = step;
    this.broadcastItem();
  };

  stepUpdate.broadcastItem = function() {
    $rootScope.$broadcast('stepUpdated');
  };

  return stepUpdate;
};
discoverModule.factory('stepUpdateService', ['$rootScope', StepUpdateService]);

function HotelService($rootScope, $http, billingDataService) {
  var hotelUpdate = { islands: ['Oahu'], hotels: []}; // hotelsByIsland object defined on fetch

  hotelUpdate.resetIslands = function(){
	hotelUpdate.islands = [];
	hotelUpdate.multipleIslands = false;
  }

  hotelUpdate.fetch = function(islandName) {
	if(typeof(hotelUpdate.hotelsByIsland) == "undefined"){
		var JsonURL = "/checkout_cart/hotels.json";
	    $http.get(JsonURL).success(hotelUpdate.setHotelsByIsland).error(function(){
		});
	}else{
		hotelUpdate.updateIslands();
	}
  }
  
  hotelUpdate.setHotelsByIsland = function(data, status){
  	hotelUpdate.hotelsByIsland = data;
	hotelUpdate.updateIslands();
  };
  
  hotelUpdate.updateIslands = function(){
  	if(typeof($rootScope.billingData) == "undefined")
  		billingDataService.define();
  	
    if(hotelUpdate.islands.length > 1){
		hotelUpdate.multipleIslands = true;
	}else if(typeof($rootScope.billingData.island) == "undefined"){
	  	$rootScope.billingData.island = hotelUpdate.islands[0]; // default to first island if only one
	}
  	
	hotelUpdate.setIslandHotels();
  };

  hotelUpdate.setIslandHotels = function(){
	if(typeof($rootScope.billingData.island) != "undefined" && $rootScope.billingData.island != null){
		hotelUpdate.hotels = hotelUpdate.hotelsByIsland[$rootScope.billingData.island.toLowerCase()];
	}else{
		delete $rootScope.billingData.island;
		delete $rootScope.billingData.hotel;
	}
  }

  return hotelUpdate;
};
discoverModule.factory('hotelService', ['$rootScope', '$http', 'billingDataService', HotelService]);

function CouponService($rootScope, $filter) {
  var couponUpdate = {totalDiscounts: 0, coupons: []};

  couponUpdate.addCoupons = function(coupons) {
	var addCount = 0;
	if(typeof(coupons) == "object"){
		for(var j = 0; j < coupons.length; j++){
			var coupon = coupons[j];
			if(typeof(coupon) != "undefined" && coupon && !couponUpdate.codeExists(couponUpdate.coupons, coupon)){
				couponUpdate.coupons.push(coupon);
				couponUpdate.broadcastAdd();
				addCount += 1;
			}
		};
	}
	
	return addCount;
  };

  couponUpdate.broadcastAdd = function() {
    $rootScope.$broadcast('couponAdded');
  };

  // check an array of coupons for a specific one
  couponUpdate.codeExists = function(coupons, givenCoupon){
  	var exists = false;
  	for(var j = 0; j < coupons.length; j++){
	    var coupon = coupons[j];
  		if(coupon.code == givenCoupon.code) exists = true;
  	};
  	return exists;
  };

  couponUpdate.processCoupons = function(){
	var saveCart = false;
	for(var j = 0; j < couponUpdate.coupons.length; j++){
	    var coupon = couponUpdate.coupons[j];
	    var index = j;
  		simpleCart.each(function (item, x) {
	        var currentItemCoupons = item.get('coupons') || [];
			
			// apply the coupon to this item if it wasn't yet applied
			if(!couponUpdate.codeExists(currentItemCoupons, coupon)){
				saveCart = true;
				currentItemCoupons.push(coupon);
	            item.set('coupons', currentItemCoupons);

	            var price = item.get('price');
				var quantity = item.get('quantity');
	            var discount = item.get('discount') || 0;
	            if(coupon.is_percentage == "1" && typeof(coupon.value) != "undefined" && typeof(coupon.code) != "undefined"){
		            discount += (price)*(coupon.value/100); // discount for one item ignoring quantity
					price = price - discount;
				    // rounding here causes cents errors $filter('currency')(price);
				}
				item.set('discount', discount);
				item.set('price', price);
			}
        });
  	};
    couponUpdate.updateDiscounts();
    if(saveCart) simpleCart.save();
  };

  couponUpdate.updateDiscounts = function(){
	    couponUpdate.totalDiscounts = 0;
		simpleCart.each(function (item, x) {
			var discount = (item.get('discount')*item.get('quantity')) || 0;
			couponUpdate.totalDiscounts += discount;
		});
  };
   
  return couponUpdate;
};
discoverModule.factory('couponService', ['$rootScope', '$filter', CouponService]);

function RegroupCartService($rootScope, $filter, $location, $routeParams, couponService, hotelService) {
  var regrouper = {};

  regrouper.regroup = function() {
	    if($rootScope.cartItemsLoaded == true){
		
        $rootScope.cartItems = [];
        var items = {};
        simpleCart.each(function (item, x) {
			couponService.addCoupons(item.get("coupons")); // if you add more the total discount neesd to increase
			
            var itemName = item.get("name");
            var nameIndex = itemName.split(' - ')[0];
            var itemDate = itemName.split(' - ')[1]; // just using the first date for now here

            if (typeof (items[nameIndex]) == "undefined" || items[nameIndex] == null || items[nameIndex].length == 0) {
                items[nameIndex] = {};
            }
            if (typeof (items[nameIndex][itemDate]) == "undefined" || items[nameIndex][itemDate] == null || items[nameIndex][itemDate].length == 0) {
                items[nameIndex][itemDate] = [];
            }

            items[nameIndex][itemDate].push(item);
        });
        hotelService.resetIslands();
        jQuery.each(items, function (activityName, dates) {
            jQuery.each(dates, function (activity_date, date_items) {
				
				var json = $rootScope.activityJSON[date_items[0].get('activity_id')];
				var packageItems = date_items[0].get('package_items');
				if(typeof(json) == "object"){
				var island = json['island'];
				if(typeof(island) != "undefined" && island != null 
				   && island.length != 0 && jQuery.inArray(island, hotelService.islands) == -1){
					hotelService.islands.push(island);
				}

                var thumbUrl = json['photo_url'];
 				var activityVariations = json['variations'];
				var cartVariations = [];
				var itemSubtotal = 0;
				var tourId = null; 
				var tourTime = null;

				jQuery.each(activityVariations, function (activityVariationIndex, variation) {
					var price = variation["price"];
					var displayPrice = price == 0.00 ? 'Free' : '$' + price;
					var quantity = 0;
					var id = null; // id of the simple cart object
					var name = variation["name"];
					var data = []; // data for the variation (ie first/last name for each traveler/quantity)

					// find the simple cart object matching the variation
					jQuery.each(date_items, function (cartItemsIndex, item) {
						if(name == item.get('size')){ 
							tourId = item.get('tour_id');
							tourTime = item.get('time');
							quantity = parseInt(item.get('quantity'));
							id = item.get('id');
							var itemData = item.get('data');
							var billingData = item.get('billing');
							if(typeof(itemData) != "undefined" && itemData != null){
								data = item.get('data');
							}
							if(typeof(billingData) == "object")
								jQuery.extend($rootScope.billingData, billingData);
						}
					});
					if(tourId == variation['tour_id']){
						itemSubtotal += quantity*price;
						
						if(data.length == 0){ // no data previously entered for this variation
							for(var i = 0; i<quantity; i++){ data.push({}); } // data from the checkout process to go right here
						}
						
						cartVariations.push({
	                        "name": name,
	                        "id": id,
	                        "price": price,
							"tourId": tourId,
	                        "display_price": displayPrice,
	                        "quantity": quantity,
							"data": data
	                    });
					}
				});
				var item = {
                    "name": activityName,
                    "date": activity_date,
                    "variations": cartVariations,
					"subtotal": itemSubtotal,
					"json": json,
					"packageItems": packageItems,
					"tourId": tourId,
					"time": tourTime,
					"thumb": thumbUrl,
                };
                regrouper.flattenTravelers(item);
                $rootScope.cartItems.push(item);
				}
            });
        });
		hotelService.fetch();
		regrouper.updateTotals();
    	regrouper.broadcastRegroup();
		$rootScope.itemCount = null;
		var pathBlank = typeof($location.path()) == "undefined" || !$location.path();
        if(pathBlank || ($location.path().indexOf('cart') == -1 && $location.path().indexOf('checkout') == -1)){
			if($rootScope.cartItems.length == 0){
				$rootScope.itemCount = "Empty";
			}
			else if($rootScope.cartItems.length >= 1){
				$rootScope.itemCount = $rootScope.cartItems.length + " Item";
				if($rootScope.cartItems.length > 1)
					$rootScope.itemCount += "s";
			}
		}
        
        $rootScope.$$phase || $rootScope.$apply(function(){
	        $rootScope.cartLoaded = true;
			if(typeof($routeParams) != "undefined" && $routeParams.step_num && $rootScope.cartItems.length == 0){
				$location.path('cart');
			}
        });
	}
  };

	// flattening the variations travelers for the purposes of a flat ng-repeat display
	regrouper.flattenTravelers = function(item){
		item.travelers = [];
		for(var i = 0; i < item.variations.length; i++){
			var variation = item.variations[i];
			for(var j = 0; j < variation.data.length; j++){
				var traveler = variation.data[j];
				traveler.variationName = variation.name;
				traveler.variationTravelerIndex = j;
				item.travelers.push(traveler);
			}
		}
	}
	
	regrouper.unflattenTravelers = function(){
		for(var i = 0; i < $rootScope.cartItems.length; i++){
			var item = $rootScope.cartItems[i];
			delete item.travelers;
		}
		$rootScope.$$phase || $rootScope.$apply();
	}

   regrouper.updateTotals = function(){
	couponService.processCoupons();
	var subTotal = parseFloat(simpleCart.total()) + parseFloat(couponService.totalDiscounts);
	var tax = parseFloat(simpleCart.tax());
	var total = (subTotal - couponService.totalDiscounts) + tax;
	$rootScope.total = $filter('split_cents')(total); // simpleCart.grandTotal() - don't like the way they calculate grand total !!
	$rootScope.subtotal = $filter('split_cents')(subTotal);
	$rootScope.discounts = $filter('split_cents')(couponService.totalDiscounts);
	$rootScope.tax = $filter('split_cents')(tax);
  };

  regrouper.broadcastRegroup = function() {
    $rootScope.$broadcast('regroupDone');
  };

  return regrouper;
}
discoverModule.factory('regroupCartService', ['$rootScope', '$filter', '$location', '$routeParams', 'couponService', 'hotelService', RegroupCartService]);