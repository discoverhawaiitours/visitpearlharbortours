function CheckoutCtrl($scope, $rootScope, $filter, $http, $location, $routeParams, stepUpdateService, regroupCartService, couponService, hotelService, billingDataService){
	$scope.formErrorMessage = 'Please complete all the required fields above.';
	$scope.bookingText = 'Complete Booking';
	$scope.processingOrder = false; // set to true when processing occurring
	$scope.selectedDate = 'not filled in';
	$scope.currentForm = null;
	$scope.defaultFailMessage = " If you are still experiencing issues, please call reservations at 808-690-9050 to complete your order.";
	$scope.months = [
					{"name" : "Jan", "num" : 1}, {"name" : "Feb", "num" : 2}, {"name" : "Mar", "num" : 3},
					{"name" : "Apr", "num" : 4}, {"name" : "May", "num" : 5}, {"name" : "Jun", "num" : 6},
					{"name" : "Jul", "num" : 7}, {"name" : "Aug", "num" : 8}, {"name" : "Sep", "num" : 9},
					{"name" : "Oct", "num" : 10 }, {"name" : "Nov", "num" : 11}, {"name" : "Dec", "num" : 12},
					];
	$scope.steps = ['one', 'two', 'thanks'];
	
	$scope.$on('stepUpdated', function() {
	  $scope.step = stepUpdateService.step; 
	});
	
	billingDataService.define(); // set the defaults for billing data
	
	// todo remove this function
	$scope.checkEmailRequired = function(field)
	{
	}
	
	$scope.addEmail = function(form){
		// only add another email if the current email is filled in and valid
		if(form.$valid){
			$rootScope.billingData.emails.push({});
		}
	}
	
	$scope.removeEmail = function(index){
		if(index != 0 && index <= ($rootScope.billingData.emails.length - 1))
			$rootScope.billingData.emails.splice(index, 1); // remove from array
	}
	
	// do the next six years on init
	$scope.initNextSixYears = function(){
		var today = new Date();
		var currentMonth = today.getMonth() + 1;
		var currentYear = today.getFullYear();
		var firstOfMonth = new Date(currentMonth + '/1/' + currentYear);
		
		if(typeof($rootScope.billingData) != "undefined" && typeof($rootScope.billingData.cc_expiration_month) != "undefined"){
			currentMonth = $rootScope.billingData.cc_expiration_month;
		}
		
		var compareDate = new Date(currentMonth + '/1/' + currentYear);
		if(compareDate < firstOfMonth){
			currentYear += 1;
		}
		$scope.nextSixYears = [];
		for(var i = 0; i < 6; i++)
			$scope.nextSixYears.push(currentYear + i);
	}
	$scope.initNextSixYears();
	
	$scope.itemRequiresMoreInfo = function(item){
		var requiresMore = false;
		var requiredFields = ["shoe_size_required", "body_weight_required", "height_required", "lunches_required"];
		for(var i = 0; i < requiredFields.length; i++){
			var field = requiredFields[i];
			if(item.json && item.json[field]){
				requiresMore = true;
			}
		}
		return requiresMore;
	};
	
	$scope.isCurrentStep = function(step) {
	  return $scope.step === step;
	};
    
	$scope.setCurrentStep = function(step) {
	  $scope.step = step;
	};
    
	$scope.getCurrentStep = function() {
	  return $scope.steps[$scope.step];
	};
	
	$scope.isFirstStep = function() {
	        return $scope.step === 0;
	};
    
	$scope.isLastStep = function() {
	    return $scope.step == $scope.steps.length - 2; // the step to submit - before the thanks step
	};
	
	$scope.isThanksStep = function() {
	    return $scope.step == $scope.steps.length - 1; // the step to submit - before the thanks step
	};
	
	$scope.areMultipleItems = function() {
		if(typeof($rootScope.cartItems) != "undefined" && $rootScope.cartItems != null)
	    	return $rootScope.cartItems.length > 1;
	};
    
	$scope.getNextLabel = function() {
	    return ($scope.isLastStep()) ? 'SUBMIT' : 'NEXT STEP'; 
	};
    
	$scope.handlePrevious = function() {
		if(!$scope.isFirstStep()){
			$location.path('checkout/step' + ($scope.step));
		}
	};
    
	$scope.handleNext = function() {
		if(typeof($scope.currentForm) != "undefined" && !$scope.currentForm.$valid){
			$scope.currentForm.$errorMessage = $scope.formErrorMessage;
			jQuery('.ng-invalid-required').removeClass('ng-pristine').addClass('ng-dirty');
			return;
		}
		$scope.currentForm.$errorMessage = null;
		if(typeof($routeParams) != "undefined" && $routeParams.step_num && $rootScope.cartItems.length == 0){
			$location.path('cart');
		}else{
			if($scope.currentForm.$valid && !$scope.isLastStep()) {
			   var stepPath = 'checkout/step' + ($scope.step + 2);
			   $location.path(stepPath);
			   $scope.saveDataToSimpleCart();
			   $scope.sendPageView();
		    }else if($scope.isLastStep()){
				if($rootScope.billingData.agree && $scope.currentForm.$valid){
					$scope.submitCheckout();
				}
			}
		}
	};
	
	$scope.saveDataToSimpleCart = function(){
		regroupCartService.unflattenTravelers(); // remove flat array of travelers (separate) from variations
		for(var itemIndex = 0; itemIndex < $scope.cartItems.length; itemIndex++){
			var item = $rootScope.cartItems[itemIndex];
			for(var j = 0; j < item.variations.length; j++){
				var variation = item.variations[j];
			    var variationIndex = j;
		        var simpleCartId = variation.id;
		        if(simpleCartId){ // simple cart item id, if quantity is 0 it won't have one
		 		   var simpleCartItem = simpleCart.find(simpleCartId);
		           if(itemIndex == 0 && variationIndex == 0){
						simpleCartItem.set('billing', $scope.santizeBillingDataForSave()); // todo strip out cc info here
				   }
		 		   simpleCartItem.set('data', variation.data);
		 		   simpleCart.save();
		 		}
		 	};
		};
	};
	
	$scope.santizeBillingDataForSave = function(){
		var data = angular.copy($rootScope.billingData);
		var banFields = ['cc_expiration_month', 'cc_expiration_year', 'cc_ccv', 'cc_number'];
		for(var i = 0; i < banFields.length; i++){
			var field = banFields[i];
			delete data[field];
		}
		return data;
	};
	
	$scope.setCurrentItem = function(item){
		$rootScope.currentItem = item;
	};
	
    $scope.initCCValidator = function(){
		CreditCardValidator.init('.ccnumber');
	};
	
	$scope.setCurrentForm = function(form){
		$scope.currentForm = form;
	};
	
	$scope.submitCheckout = function(){
	    $scope.checkoutUrl = "//" + document.domain + "/checkout/process";
	    var data = {billingData: $scope.billingData, cartItems: $scope.cartItems, coupons: couponService.coupons};
	    $scope.processingOrder = true;
	    $scope.bookingText = "Processing Order ...";
		$http.post($scope.checkoutUrl, data).success($scope.successfulCheckout).error(function(data, status, headers, config) {
		    $scope.failCheckout();
		});
	};
	
	$scope.successfulCheckout = function(data){
		if(typeof(data.authorize) == "object" && typeof(data.authorize.responsereasontext) != "undefined"){
			if(data.authorize.responsereasoncode == "1"){
				$scope.step += 1;
				$rootScope.finalBillingData = angular.copy($rootScope.billingData);
				$rootScope.finalCartItems = angular.copy($rootScope.cartItems);
				$rootScope.finalSubtotal = angular.copy($rootScope.subtotal);
				$rootScope.finalTotal = angular.copy($rootScope.total);
				$rootScope.finalTax = angular.copy($rootScope.tax);
				$rootScope.finalDiscounts = angular.copy($rootScope.discounts);

				ga('ecommerce:addTransaction', {
				  'id': data.authorize.transactionid,                     // Transaction ID. Required.
				  'affiliation': 'Pearl Harbor Oahu',   // Affiliation or store name.
				  'revenue': data.billingData.subTotal,               // Grand Total.
				  'shipping': '0',                  // Shipping.
				  'tax': data.billingData.tax                   // Tax.
				});
				for (var i = 0; i < data.cartItems.length; i++) {
					var thisItem = data.cartItems[i];
					ga('ecommerce:addItem', {
					  'id': data.authorize.transactionid,                     // Transaction ID. Required.
					  'name': thisItem.name,    // Product name. Required.
					  'sku': thisItem.tourId,                 // SKU/code.
					  'category': 'Tours',         // Category or variation.
					  'price': thisItem.subtotal,                 // Unit price.
					  'quantity': thisItem.travelers.length                   // Quantity.
					});
				};
				ga('ecommerce:send');

				/*
				var google_conversion_id = 984763046;
				var google_conversion_language = "en";
				var google_conversion_format = "3";
				var google_conversion_color = "ffffff";
				var google_conversion_label = "65rwCIq19gQQppXJ1QM";
				var google_conversion_value = parseFloat(data.authorize.amount);
				var google_remarketing_only = false;
				$scope.async('//www.googleadservices.com/pagead/conversion.js');*/

				$scope.currentForm.$wasCardError = false;
				simpleCart.empty();
				simpleCart.save();
				$scope.$$phase || $scope.$apply();
				$scope.sendPageView('checkout/thanks');
				$location.path('checkout/thanks');
			}else{
				$scope.failCheckout("Sorry, there were problems processing your card. " + data.authorize.responsereasontext + " ");
			}
		}else{
			$scope.failCheckout("Sorry, there were problems processing your card. ");
		}
	};
	
	$scope.async = function(url){
			//this is just a utility function to load external scripts async
				var s = document.createElement('script');
   				s.type = 'text/javascript';
    			s.async = true;
    			s.src = url;
   				var x = document.getElementsByTagName('script')[0];
    			x.parentNode.insertBefore(s, x);
	};


	$scope.failCheckout = function(msg){
		if(typeof(msg) != "string"){
			msg = "Sorry, your order could not be processed at this time. Please check your payment details and try again.";
		}
		msg = msg + $scope.defaultFailMessage;
		jQuery('#submitfailure').html('<p>'+msg+'</p>');
		jQuery('#submitfailure').slideDown();
		var copiedData = angular.copy($scope.billingData);
		delete copiedData.cc_number;
		delete copiedData.cc_expiration_month;
		delete copiedData.cc_expiration_year;
		delete copiedData.cc_ccv;
		$scope.processingOrder = false;
		$scope.bookingText = 'Complete Booking';
		$scope.currentForm.$errorMessage = msg;
		//$scope.currentForm.$setValidity(false);
		$scope.currentForm.$wasCardError = true;
		$scope.$$phase || $scope.$apply();
	};
	
	$scope.isCheckout = true;
	
	$scope.setStartStep = function(){
		if($routeParams.step_num){
			var stepNum;
			if($routeParams.step_num.indexOf('thanks') == -1){
				stepNum = parseInt($routeParams.step_num.replace('step', ''));
			}else{
				if(typeof($rootScope.finalTotal) != "undefined" && $rootScope.finalTotal){	
					stepNum = $scope.steps.length; // go to last step, non-zero index to match path indexing
				}else{
					return $location.path('cart');
				}
			}
			if(stepNum && stepNum <= $scope.steps.length){
				$scope.step = stepNum - 1;
			}
		}else{
			$scope.step = 0;
			$location.path('checkout/step1');
		}
	};
	$scope.setStartStep();
	
	$scope.maskCC = function($event){
		var value = jQuery($event.currentTarget).inputmask("unmaskedvalue");
		var parsedCCNumber = value.replace(/ /g,"");
		$scope.currentForm.ccnumber.$setViewValue(parsedCCNumber); // fix view value from input mask
		jQuery(".cards li").removeClass('on');
		if(parsedCCNumber.length > 15){
			jQuery($event.currentTarget).validateCreditCard(function(result){
				$scope.onValidateCreditCard(result, '.ccnumber', $event.currentTarget);
			});
		}
	};
	
	$scope.setSelectedCard = function(element, cardName){
		if(typeof(element) != "undefined" && typeof(cardName) == "string"){
			//todo tab these out right after entry
			//var maskStr = cardName == "amex" ? "9999 999999 99999" : "9999 9999 9999 9999";
			//jQuery(element).inputmask(maskStr,{ "placeholder": " " });
			jQuery("li." + cardName).addClass('on');
		}
	};
	
	$scope.onValidateCreditCard = function(result, selector, element){
        if((result.card_type && result.length_valid && result.luhn_valid)){
	        var cardName = result.card_type.name;
	        $scope.setSelectedCard(element, cardName);
            jQuery(element).removeClass('ng-invalid').addClass('ng-valid').removeClass('ng-pristine').addClass('ng-dirty');
            jQuery(element).removeClass('ng-invalid-required').addClass('ng-valid-required');
            angular.element(selector).scope().currentForm.ccnumber.$valid = true;
            angular.element(selector).scope().$$phase || angular.element(selector).scope().$apply();
        }else{
            jQuery(element).addClass('ng-invalid').removeClass('ng-valid');
        }
	};
    
	$scope.maskPhone = function($event){
		var value = jQuery($event.currentTarget).inputmask("unmaskedvalue").replace(/ /g,"");
		$scope.currentForm.cell.$setViewValue(value); // fix problem with jquery plugin not setting view value
	};
	
	$scope.zips = []; // cache the zips to prevent dup requests
	$scope.zipLookup = function($event){
		$event.preventDefault();
		var city, state, country;
        currentValue = $rootScope.billingData.zip;
        if(typeof(currentValue) != "undefined" && currentValue.length >= 4 && $event.which != 69){
	        if(typeof($scope.zips[$rootScope.billingData.country]) != "undefined" && 
	           $scope.zips[$rootScope.billingData.country][currentValue]){
		        city = $scope.zips[$rootScope.billingData.country][currentValue].city;
		        state = $scope.zips[$rootScope.billingData.country][currentValue].state;
				$scope.setCityState(city, state);
	        }else{
				var url = "/checkout/zip/json?address=" + currentValue + "&sensor=false";
				$http.get(url).success(function(data){
					if(data.status == "OK" && data.results.length > 0){
						var components = data.results[0].address_components;
						for(var i = 0; i < components.length; i++){
							var component = components[i];
							var types = component.types;
							if(jQuery.inArray('administrative_area_level_1', types) != -1){ // this is a state
								state = component.short_name;
							}else if(jQuery.inArray('locality', types) != -1){
								city = component.long_name;
							}else if(jQuery.inArray('country', types) != -1){
								country = component.long_name;
							}
						}
						if(!city || !state || !country){
							$scope.clearCityState();
						}else if(country == $rootScope.billingData.country){
							$scope.setCityState(city, state);
							if(typeof($scope.zips[country]) == "undefined"){
								$scope.zips[country] = [];
							}
							$scope.zips[country][currentValue] = {"city": city, "state": state};
						}
					}else{
						$scope.clearCityState();
					}
				}).error(function(){
					$scope.clearCityState();
				});
	        }
		}
	}
	
	$scope.setCityState = function(city, state){
		$scope.currentForm.state.$setViewValue(''); // make the field dirty
		$rootScope.billingData.state = state;
		$scope.currentForm.city.$setViewValue(''); // make the field dirty
		$rootScope.billingData.city = city;
	}
	
	$scope.clearCityState = function(){
		$scope.currentForm.city.$setPristine();
		$scope.currentForm.state.$setPristine();
		$rootScope.billingData.state = null;
		$rootScope.billingData.city = null;
	}
	
	$scope.changeCountry = function(){
		$rootScope.billingData.zip = null;
		$scope.currentForm.zip.$setPristine();
		$scope.clearCityState();
	}
	
	$scope.sendPageView = function(path){
		var title = 'Checkout Step ' + ($scope.step + 1);
		var page = "/checkout/#/";
		if(typeof(path) == "undefined"){
			page += 'checkout/step' + ($scope.step + 1);
		}else{
			if(path.indexOf('thanks') != -1){
				title += ' - Confirmation Page';
			}
			page += path;
		}
		ga('send', 'pageview', {
		  'page': page,
		  'title': title
		});
	}
	$scope.sendPageView();

	$scope.hotelService = hotelService;
	regroupCartService.regroup();
}
CheckoutCtrl.$inject = ['$scope', '$rootScope', '$filter', '$http', '$location', '$routeParams', 'stepUpdateService', 'regroupCartService', 'couponService', 'hotelService', 'billingDataService'];
discoverModule.controller('CheckoutCtrl', CheckoutCtrl);