function AppCtrl($scope, $rootScope, $http, couponService, regroupCartService){ 
	$rootScope.checkoutLink = "/checkout/#/cart";
	if(document.domain == "www.discoverhawaiitours.com" || document.domain == "discoverhawaiitours.com"){
		$rootScope.checkoutLink = "https://" + document.domain + $rootScope.checkoutLink;
	}
    $scope.init = function(){
	    if(typeof($rootScope.appInitted) == "undefined"){
	    	$rootScope.activityJSON = {};
	    	simpleCart.extend({save: $scope.setCookie, load: function(){}});
	    	simpleCart({
				cartStyle: "div", 
				checkout: { 
					type: "SendForm" 
				},
				currency: "USD",
				data: {},
				language: "english-us",
				excludeFromCheckout: [],
				shippingCustom: null,
				shippingFlatRate: 0,
				shippingQuantityRate: 0,
				shippingTotalRate: 0,
				taxRate: .0775,
				taxShipping: false
			});
			$scope.getCookie();
			$rootScope.appInitted = true;
		}
	};
	
	$scope.hasDiscount = function(){
	    var hasDiscount = false;
	    if(couponService.totalDiscounts > 0){
			hasDiscount = true;
		}
		return hasDiscount;
	};

	$scope.getCookie = function(){
		if(typeof($rootScope.cartItemsLoaded) == "undefined"){
			var activityIds = []; // wordpress activity ids needed to loaded the json
	        var ids = jQuery.cookie("simplecart_items"); // index of all simplecart ids

		        if(typeof(ids) == "undefined" || ((typeof(ids) == "string") && ids.length == 0)){
					$rootScope.cartItemsLoaded = true;
					regroupCartService.regroup();
					return;
				}
	            simpleCart.each(ids.split(","), function (id) {
					
		            var data = jQuery.cookie("simplecart_items_" + id);
		            if(typeof(data) == "string" && data.length != 0){
						var simpleCartJSON = JSON.parse(data);
						var simpleCartId = simpleCartJSON.id;
						var activityId = simpleCartJSON.activity_id;
						if(simpleCart.find(simpleCartId).length == 0){
							simpleCart.add(simpleCartJSON, true);
							if(jQuery.inArray( simpleCartJSON.activity_id, activityIds) == -1){ 
								activityIds.push(simpleCartJSON.activity_id);
							}
						}
					}
	            });
	        $scope.loadActivities(activityIds, function(){regroupCartService.regroup()});
		}
	};
	
	$scope.loadActivities = function(activityIds, callback){
		// load the first json then pass the array and callback back to the function
		if(activityIds.length > 0){
			var id = activityIds.pop();
			var nextCallback = function(){ $scope.loadActivities(activityIds, callback); };
			$scope.loadActivityJSON(id, nextCallback);
		}else if(typeof(callback) == "function"){ // there are no more activity jsons to pull
			$rootScope.cartItemsLoaded = true;
			callback();
		}
	}
	
	// todo: should really be a service to avoid duplicate code
	$scope.loadActivityJSON = function(activityId, callback){
		if(typeof(callback) != "function"){
			callback = function(){}
		}
		var successLoadJSON = function(json){
			$rootScope.activityJSON[activityId] = json;
			callback();
		}
		if (typeof (activityId) != "undefined") {
		    var activityUrl = "//s3.amazonaws.com/mirror.discoverhawaiitours.com/staging_activity_object_json/" +
		 				      activityId + ".json";
		    $http.get(activityUrl).success(successLoadJSON).error(function () {
		        // try to load the cart via proxy
		        $scope.proxyActivityUrl = "//" + document.domain + "/checkout/json/staging_activity_object_json/" + 
										  activityId + ".json";
		        $http.get($scope.proxyActivityUrl).success(successLoadJSON).error(function(){
					callback(); // return even if you fail
				});
		    });
			$scope.$$phase || $scope.$apply(); // force angular to do request
		} else {
			console.log("there was an error loading the json");
			callback();
		}
	}
	
	// changing this to use multiple cookies
	$scope.setCookie = function(){
		if($rootScope.cartItemsLoaded == true){
			simpleCart.trigger('beforeSave');
        	var ids = [];
        	jQuery.cookie.raw = true;
        	simpleCart.each(function (item) {
	    	    ids.push(item.id());
	    	    var cookieContent = JSON.stringify(simpleCart.extend(item.fields(), item.options()));
	    	    jQuery.cookie("simplecart_items_" + item.id(), cookieContent, { expires: 1, path: '/' });
        	});
			jQuery.cookie("simplecart_items", ids.join(','), { expires: 1, path: '/' });
        	simpleCart.trigger('afterSave');
		}
	};
	$scope.init();
}
AppCtrl.$inject = ['$scope', '$rootScope', '$http', 'couponService', 'regroupCartService'];
discoverModule.controller('AppCtrl', AppCtrl);