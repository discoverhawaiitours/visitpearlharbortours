function BookCtrl($scope, $rootScope, $http, $routeParams, $filter, $location){ 
  $scope.calendarId = "book-cal";
  $scope.isPackageItem = false;

  // check the quantities to make sure at least one person is going
  $scope.quantityMet = true; // true because 1 is selected for first variation
  $scope.validateQuantities = function(){
	$scope.quantityMet = false;
	if(typeof($scope.tourObj) != "undefined"){
		for(var i = 0; i < $scope.tourObj.variations.length; i++){
			var variation = $scope.tourObj.variations[i];
			if(variation.quantity > 0)
				$scope.quantityMet = true;
		}
	}
  };

  $scope.isValidForm = function(form){
	var valid = true;
	if(!form.$valid || !$scope.quantityMet){
		valid = false;
	}
	return valid;
  };

  $scope.departureIslands = ["Oahu", "Big Island", "Maui"];
  $scope.pid = $routeParams.activity_id || activity_id;

  // receives the timeSeperate object, converts to military time
  $scope.toMilitaryTime = function(input) {
	 if(input.toLowerCase() == 'varies')
		return input;
	  var time = input.split(":");
      var hours = time[0]; 
      var mins = time[1]; 
      mins = mins.split(" "); 
      minutes = mins[0]; 
      var ampm = mins[1]; 
      timeObj = {"hours":hours, "minutes":minutes, "ampm":ampm};

        var militaryHours;
        if( timeObj.ampm == "am" ) {
            militaryHours = timeObj.hours;
            // check for special case: midnight
            if (militaryHours.length < 2) { militaryHours = "0" + militaryHours; }
            else if( militaryHours == "12" ) { militaryHours = "00"; }
        } else {
            // if( ampm == "pm") {
                // get the interger value of hours, then add
                tempHours = parseInt( timeObj.hours ) + 2;
                // adding the numbers as strings converts to strings
                if (tempHours < 10 ) {
                    tempHours = "1" + tempHours;
                } else {
                    tempHours = "2" + ( tempHours - 10 );
                }
                // check for special case: noon
                if( tempHours == "24" ) { tempHours = "12"; }
                militaryHours = tempHours;
        }
        return militaryHours + timeObj.minutes;
    }


  $scope.checkAvailability = function(){
	var time = $scope.tourObj.departureTime;  
	var militaryTime = $scope.toMilitaryTime(time);
	ActivityCal.postForAvailability(time, militaryTime);
  }

  $scope.setTourId = function(tourId){
	 $scope.tourObj.tourId = tourId;
	 var visibleVariations = []; // todo use the filter here duh
	for(var i = 0; i < $scope.tourObj.variations.length; i++){
		var variation = $scope.tourObj.variations[i];
		$scope.checkTimeVariation(variation);
		if(variation.visible)
			visibleVariations.push(variation);
	}
	 for(var i = 0; i < $scope.tourObj.times.length; i++){
			var time = $scope.tourObj.times[i];
			if(time.tourId == tourId)
				$scope.tourObj.departureTime = $scope.tourObj.times[i].departure;
	 }
	 for(var i = 0; i < visibleVariations.length; i++){
		var variation = visibleVariations[i];
		$scope.initVariationQuantity(variation, i)
	 }
  };

    $scope.insertInfo = function(data, status){
      $scope.tourObj = data;
	  $scope.tourObj.departureTime = $scope.tourObj.times[0].departure;
	  $scope.tourObj.departureIsland = $scope.departureIslands[0];
	
      $scope.setTourId($scope.tourObj.times[0].tourId);
      
	  var calOptions = {
	 	 "elementId": $scope.calendarId,
	  	 "activityId": $scope.pid,
	  	 "processAvailability": true,
	  }
	  ActivityCal.init(calOptions);
	  $scope.isPackageItem = $scope.tourObj.package_items.length != 0;
      $scope.checkAvailability();
    }

    $scope.fetchInfo = function() {
	  try{
		if (typeof ($scope.pid) != "undefined") {
		    $scope.activityUrl = "//s3.amazonaws.com/mirror.discoverhawaiitours.com/staging_activity_object_json/" + $scope.pid + ".json";
		    $http.get($scope.activityUrl).success($scope.insertInfo).error(function () {
		        // try to load the cart via proxy
		        $scope.proxyActivityUrl = "//" + document.domain + "/checkout/json/staging_activity_object_json/" + $scope.pid + ".json";
		        $http.get($scope.proxyActivityUrl).success($scope.insertInfo).error(function(){
				});
		    });
		} else {
		    console.log("no pid");
		}
	  }catch(Error){
		console.log("there was an error loading the json");
	  }
	  
    }

    $scope.checkTimeVariation = function(variation){
		variation.visible = variation.tour_id == $scope.tourObj.tourId ? true : false;
	};
	
	$scope.initVariationQuantity = function(variation, index){
		variation.quantity = index == 0 ? 1 : 0;
	};
	
	$scope.initNewCalendar = function(calId){
		$scope.calendarId = calId;
	};

    //initialize everything
    $scope.fetchInfo();
}
BookCtrl.$inject = ['$scope', '$rootScope', '$http', '$routeParams', '$filter', '$location'];
discoverModule.controller('BookCtrl', BookCtrl);