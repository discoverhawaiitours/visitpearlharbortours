<?php 
 $external_scripts = array('//code.jquery.com/jquery-1.10.2.min.js', '//cdnjs.cloudflare.com/ajax/libs/hammer.js/1.0.5/jquery.hammer.min.js', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.min.js', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular-sanitize.min.js', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular-resource.min.js', '//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.6.0/ui-bootstrap-tpls.min.js');
 $scripts = array('/checkout/static_site/js/bootstrap.min.js','/checkout/static_site/js/simpleCart.min.js', '/checkout/static_site/js/fieldmask.js', '/checkout/static_site/js/angular-placeholder-shim.min.js', '/checkout/static_site/js/jquery.html5-placeholder-shim.js', '/checkout/static_site/js/calendar-1.5.min.js','/checkout/static_site/js/activity_cal.js','/checkout/static_site/js/cookie.js','/checkout/static_site/js/validate_credit_card.js','/checkout/static_site/js/angular_init.js','/checkout/static_site/js/filters.js','/checkout/static_site/js/services.js','/checkout/static_site/js/routes.js','/checkout/static_site/js/directives.js','/checkout/static_site/js/controllers/AppCtrl.js','/checkout/static_site/js/controllers/BookCtrl.js','/checkout/static_site/js/controllers/CartCtrl.js','/checkout/static_site/js/controllers/CheckoutCtrl.js', '/checkout/static_site/js/controllers/CouponCtrl.js');

 echo "<link rel='stylesheet' type='text/css' href='/checkout/static_site/css/app.css'>";

 foreach ($external_scripts as $script) {
    echo '<script src="'.$script.'"></script>';
  }

  foreach ($scripts as $script) {
    echo '<script src="'.$script.'"></script>';
  }
  

?>

<div ng-app="discoverModule" id="ng-app" class="ng-scope" ng-controller="AppCtrl">             
<div class="sidebar-calendar" ng-include="'CalWrapper.html'"></div>

<script type="text/ng-template" id="CalWrapper.html" data-cdata="true">
    <?php include_once(__DIR__.'/views/cal_wrapper.html'); ?>
</script>

<script type="text/ng-template" id="Calendar.html" data-cdata="true">
    <?php include_once(__DIR__.'/views/calendar.html'); ?>
</script>

<script type="text/ng-template" id="ActivityTimes.html" data-cdata="true">
    <?php include_once(__DIR__.'/views/activity_times.html'); ?>
</script>

<script type="text/ng-template" id="ActivityPrices.html" data-cdata="true">
    <?php include_once(__DIR__.'/views/activity_prices.html'); ?>
</script>
<script type="text/ng-template" id="AddToCart.html" data-cdata="true">
    <?php include_once(__DIR__.'/views/add_to_cart_form.html'); ?>
</script>
</div>