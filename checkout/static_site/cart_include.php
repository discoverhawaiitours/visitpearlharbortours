<?php 
  $external_scripts = array('//code.jquery.com/jquery-1.10.2.min.js', '//cdnjs.cloudflare.com/ajax/libs/hammer.js/1.0.5/jquery.hammer.min.js', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.min.js', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular-sanitize.min.js', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular-resource.min.js', '//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.6.0/ui-bootstrap-tpls.min.js');
  $scripts = array('/checkout/static_site/js/bootstrap.min.js','/checkout/static_site/js/simpleCart.min.js', '/checkout/static_site/js/fieldmask.js', '/checkout/static_site/js/angular-placeholder-shim.min.js', '/checkout/static_site/js/jquery.html5-placeholder-shim.js', '/checkout/static_site/js/calendar-1.5.min.js','/checkout/static_site/js/activity_cal.js','/checkout/static_site/js/cookie.js','/checkout/static_site/js/validate_credit_card.js','/checkout/static_site/js/angular_init.js','/checkout/static_site/js/filters.js','/checkout/static_site/js/services.js','/checkout/static_site/js/routes.js','/checkout/static_site/js/directives.js','/checkout/static_site/js/controllers/AppCtrl.js','/checkout/static_site/js/controllers/BookCtrl.js','/checkout/static_site/js/controllers/CartCtrl.js','/checkout/static_site/js/controllers/CheckoutCtrl.js', '/checkout/static_site/js/controllers/CouponCtrl.js');
  echo "<link rel='stylesheet' type='text/css' href='/checkout/static_site/css/app.css'>";
  
    foreach ($external_scripts as $script) {
    echo '<script src="'.$script.'"></script>';
  }

  foreach ($scripts as $script) {
    echo '<script src="'.$script.'"></script>';
  }
  

?>


<div id="ng-app" ng-app="discoverModule" ng-controller="AppCtrl">
<div ng-view style="position: relative" class="cartViewWrap">
  <div class="loadingcart" style="width: 100%">
    <div class="carttitle">Please wait your cart is loading ...</div>
    <img src="//cdn.discoverhawaiitours.com/wp-content/themes/theme_master/assets/img/omar_ajax_load.gif" />
  </div>
</div>

<script type="text/ng-template" id="Cart.html" data-cdata="true">
    <?php include_once(__DIR__ . "/views/cart.html"); ?>
</script>
<script type="text/ng-template" id="Checkout.html" data-cdata="true">
    <?php include_once(__DIR__ . "/views/checkout.html"); ?>
</script>
<script type="text/ng-template" id="Calendar.html" data-cdata="true">
    <?php include_once(__DIR__ . "/views/calendar.html"); ?>
</script>
<script type="text/ng-template" id="TotalCheckout.html" data-cdata="true">
    <?php include_once(__DIR__ . "/views/checkout/totalcheckout.html"); ?>
</script>
<script type="text/ng-template" id="TotalCart.html" data-cdata="true">
    <?php include_once(__DIR__ . "/views/checkout/totalcart.html"); ?>
</script>
<script type="text/ng-template" id="Steps.html" data-cdata="true">
    <?php include_once(__DIR__ . "/views/checkout/steps.html"); ?>
</script>
<script type="text/ng-template" id="Step1.html" data-cdata="true">
    <?php include_once(__DIR__ . "/views/checkout/step1.html"); ?>
</script>
<script type="text/ng-template" id="Step2.html" data-cdata="true">
    <?php include_once(__DIR__ . "/views/checkout/step2.html"); ?>
</script>
<script type="text/ng-template" id="Step3.html" data-cdata="true">
    <?php include_once(__DIR__ . "/views/checkout/step3.html"); ?>
</script>
</div>